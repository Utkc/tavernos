﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePlaceAddWood : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnMouseDown()
    {
        Debug.Log("Add Wood Clicked");
        gameObject.GetComponentInParent<Fireplace>().AddSingleWood();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
