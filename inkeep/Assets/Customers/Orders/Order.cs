﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Order : MonoBehaviour
{
    public Sprite OrderImage;

    public Sprite BreadOrderSprite;
    public Sprite FishOrderSprite;
    public Sprite BeerOrderSprite;

    public int beerPrice;
    public int breadPrice;
    public int fishPrice;


    public Customers.OrderType order;

    //public float WaitTimer;
    public WaitTimer TimerBar;



    private void OnMouseDown()
    {
        order = gameObject.GetComponentInParent<CustomerData>().Data.order;

        Debug.Log(" Stuff Has Been Served");
        if (order == Customers.OrderType.Bread)
            Debug.Log("Need Bread " + order.ToString() );
        if (order == Customers.OrderType.Beer)
            Debug.Log("Need Beer " +order.ToString() );
        if (order == Customers.OrderType.Fish)
            Debug.Log("Need Fish" + order.ToString() );
        // Orders need rework
        if( StatManager.Stats.isConsumableBy(order.ToString(),gameObject.GetComponentInParent<CustomerData>().Data.HowManyToEat ) )
        {
            if (order == Customers.OrderType.Bread)
            {
                GameObject.Find(order.ToString() + "Station").GetComponent<StationManager>().consumeStorable();
                StatManager.Stats.ConsumeItemBy(order.ToString(), gameObject.GetComponentInParent<CustomerData>().Data.HowManyToEat);
                StatManager.Stats.IncreaseItemBy("Gold", gameObject.GetComponentInParent<CustomerData>().Data.HowManyToEat * breadPrice);
            }
            else if (order == Customers.OrderType.Beer)
            {
                Debug.Log(order.ToString() + "Station");
                GameObject.Find(order.ToString() + "Station").GetComponent<StationManager>().consumeStorable();
                StatManager.Stats.ConsumeItemBy(order.ToString(), gameObject.GetComponentInParent<CustomerData>().Data.HowManyToEat);
                StatManager.Stats.IncreaseItemBy("Gold", gameObject.GetComponentInParent<CustomerData>().Data.HowManyToEat * beerPrice);
            }
            else if (order == Customers.OrderType.Fish)
            {
                GameObject.Find(order.ToString() + "Station").GetComponent<StationManager>().consumeStorable();
                StatManager.Stats.ConsumeItemBy(order.ToString(), gameObject.GetComponentInParent<CustomerData>().Data.HowManyToEat);
                StatManager.Stats.IncreaseItemBy("Gold", gameObject.GetComponentInParent<CustomerData>().Data.HowManyToEat * fishPrice);
            }
            else
            {
                Debug.LogError("Something Worng with orther type");
            }

            Debug.Log("Order Recieved... Consumed item is " + order.ToString());
            Debug.Log("Order in Data Scheme " + gameObject.GetComponentInParent<CustomerData>().Data.order );
            gameObject.GetComponentInParent<Puppeteer>().OrderRecieved( gameObject.GetComponentInParent<CustomerData>().Data.order );
        }
        else
        {
            Debug.Log(" YOU DONT HAVE THE FOOD . Keep Waiting ?");
            // cant consume visuals goes here
        }

    }

    public void endOrder()
    {
        GameObject.Destroy(gameObject);
    }

    public void SetupOrderSprites(Customers.OrderType ordr )
    {
        Debug.Log("order start waiting");
        // Orders need rework
        if (ordr == Customers.OrderType.Bread)
            gameObject.GetComponent<SpriteRenderer>().sprite = BreadOrderSprite;
        else if (ordr == Customers.OrderType.Beer)
            gameObject.GetComponent<SpriteRenderer>().sprite = BeerOrderSprite;
        else if (ordr == Customers.OrderType.Fish)
            gameObject.GetComponent<SpriteRenderer>().sprite = FishOrderSprite;
        else
        {
            Debug.Log("none ?! "+ ordr);
            gameObject.GetComponent<SpriteRenderer>().sprite = OrderImage;
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
