﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Empty Customer", menuName = "Customers/New Customer")]
public class Customers : ScriptableObject
{

    public enum Factions {Scholers,Order,Thieves}
    public enum MemberType {Peasant,Member,Knight}

    public string customerName;

    public Factions customerType;
    public MemberType customerFaction;
    public string curState;

    public enum OrderType { Bread, Fish, Beer }
    public OrderType order;
    public int HowManyToEat;
    public float HowLongShouldIWaitForService;

    public string curAnimationState;
    public GameObject CustomerBody;

    public int RepDown;
    public int RepUp;
    public float HowSlowIEat;

    public bool ShouldIGoToCounter;

    public string Go2CounterStartThisDialog;


    public string EventId;
    public List<string> DialogTexts;

    public string SmallTalkPoolName;

    public bool IsItCounterCustomer()
    {
        return ShouldIGoToCounter;
    }

    public bool IsItDialogCustomer()
    {
        if (Go2CounterStartThisDialog != "" && Go2CounterStartThisDialog != null)
            return true;
        else
            return false;
    }

    public void recordSelf()
    {
        //Debug.Log("recorded işte");
        Catalogue.Singleton.recordaType(this);
    }

}
