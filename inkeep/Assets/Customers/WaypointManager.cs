﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointManager : MonoBehaviour
{
    public Waypoint CounterWP;
    public Waypoint TavernEnterence;
    public static WaypointManager Singleton;
    public List<GameObject> wayPoints = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }

}
