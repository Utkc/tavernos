﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Catalogue : MonoBehaviour
{
    public static Catalogue Singleton;

    //insert all customer scriptable object here.
    public List<Customers> lst = new List<Customers>();

   
    public Dictionary<Customers,GameObject> allTypes = new Dictionary<Customers,GameObject>();
    
    public void recordaType(Customers cs)
    {
        //Debug.Log("recorded");
        allTypes.Add(cs,cs.CustomerBody);
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in lst)
        {
           item.recordSelf();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }

}
