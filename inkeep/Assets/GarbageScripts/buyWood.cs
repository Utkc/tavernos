﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buyWood : MonoBehaviour
{
    public int woodPrice;

    public void buyWoodbutton (int amnt)
    {
        if (StatManager.Stats.isConsumableBy("Gold", amnt * woodPrice) )
        {
            StatManager.Stats.IncreaseItemBy("Wood", amnt );
            StatManager.Stats.ConsumeItemBy("Gold", amnt * woodPrice);
        }
        else if ( !(StatManager.Stats.isConsumableBy("Gold", amnt * woodPrice)) )
        {
            Debug.Log("not enough money");
        }
        else if ( !(StatManager.Stats.isConsumableBy("Wood", amnt)) )
        {
            Debug.Log("not enough Wood");
        }
        else
        {
            Debug.Log("etf");
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
