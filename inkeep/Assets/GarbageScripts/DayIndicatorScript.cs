﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayIndicatorScript : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<Text>().text = " Day#"+DayCycle.Singleton.TheDay.ToString()+" Time:"+ DayCycle.Singleton.DayPercent+"%";
    }
}
