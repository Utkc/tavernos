﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class MainMenuButton : MonoBehaviour ,IPointerEnterHandler,IPointerExitHandler
{
    private TextMeshProUGUI tmpObj;
    // Start is called before the first frame update
    void Start()
    {
        tmpObj = gameObject.GetComponent<TextMeshProUGUI>();
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("hovered");
        //Debug.Log(gameObject.GetComponent<TextMeshProUGUI>().faceColor);
        tmpObj.overrideColorTags = true;
        tmpObj.faceColor = new Color32(255,255,255,255);
        gameObject.GetComponent<TextMeshProUGUI>().faceColor = new Color32(255, 255, 255, 255);
        //Debug.Log(gameObject.GetComponent<TextMeshProUGUI>().faceColor);
        //gameObject.GetComponent<TextMeshProUGUI>().faceColor = new Color32(255, 255, 255, 255);

    }

    public void OnPointerExit(PointerEventData eventData)
    {

        //gameObject.GetComponent<TextMeshProUGUI>().faceColor = new Color(255f, 255f, 255f, 255f);
        //gameObject.GetComponent<TextMeshProUGUI>().color = new Color(255f, 255f, 255f, 255f);
        Debug.Log("exited");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
