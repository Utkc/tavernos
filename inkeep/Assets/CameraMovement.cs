﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    float camSlideSpeedX = 5f;
    [SerializeField]
    float camSlideSpeedY = 5f;
    //move camera with mouse screen position
    void Start()
    {
        transform.position = new Vector3(0f,0f, -17f);
    }
    /*
    void Update()
    {
        
        float xPos = Mathf.Clamp(Input.GetAxisRaw("Mouse X"), -20f, +20f);
        float yPos = Mathf.Clamp(Input.GetAxisRaw("Mouse Y"), -20f, +20f);
        transform.position += new Vector3(xPos * Time.deltaTime * camSlideSpeedX, yPos * Time.deltaTime * camSlideSpeedY, 0f);
        
    }*/
    public float dragSpeed = 2;
    private Vector3 dragOrigin;


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }

        if (!Input.GetMouseButton(0)) return;

        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(pos.x * dragSpeed, pos.y * dragSpeed, 0 );

        float xPos = Mathf.Clamp(pos.x, -20f, +20f);
        float yPos = Mathf.Clamp(pos.y, -20f, +20f);
        //switch y with x axis

        transform.Translate(move, Space.World);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -7f, +6f), Mathf.Clamp(transform.position.y, -3.5f, +4f) , -17f );
    }
}
