﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    bool AnimsEnded = true;
    bool CreditsOn = false;

    public void LoadGameSceneAnim()
    {
        gameObject.GetComponentInParent<Animator>().Play("HideMenu");
    }
    public void SwitchScene()
    {
        SceneManager.LoadScene(1);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && CreditsOn)
        {
            HideCredits();
        }
        
    }
    public void SetAnimsEndedTrue()
    {
        AnimsEnded = true;
    }
    public void SetAnimsEndedFalse()
    {
        AnimsEnded = false;
    }
    public void ShutDown()
    {
        Application.Quit();
    }
    public void ShowCredits()
    {
        Debug.Log("a " + CreditsOn + " b " + AnimsEnded);
        if (!CreditsOn && AnimsEnded)
        {
            CreditsOn = true;
            gameObject.GetComponentInParent<Animator>().Play("CreditsShow");
        }
        else if (CreditsOn && AnimsEnded)
        {
            HideCredits();
        }
    }
    public void HideCredits()
    {
        //start only if all animations ended
        if (CreditsOn && AnimsEnded)
        {
            CreditsOn = false;
            gameObject.GetComponentInParent<Animator>().Play("CreditsHide");
        }
    }


}
