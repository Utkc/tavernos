﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UI;
using UnityEngine;

public class customEffects : MonoBehaviour
{

    public static customEffects Itself;


    void Awake()
    {
        if (Itself != null)
            GameObject.Destroy(Itself);
        else
            Itself = this;

    }


    // Start is called before the first frame update
    void Start()
    {
         
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void customEffectInvoker(string effectName)
    {
        Invoke(effectName, 0.0f);
    }

        
    public void something2execute()
    {
        Debug.Log("something is executed");
    }
}
