﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public GameObject EventManagerGO;
    //public Image selfimage;
    public string selftext;
    public List<ASingleEffect> effectlst;

    public void setButtonWithSingleChoice(SingleChoice aChoiceFromSingleEvent)
    {
        if(aChoiceFromSingleEvent.EffectList.Count > 0)
        {
            recieveEffectList(aChoiceFromSingleEvent.EffectList);
        }
        else
        {
            // well there might be many choices with no effects. no need to flood warnings
            //Debug.LogWarning("A single Choice does not have any effects");
        }

        if (aChoiceFromSingleEvent.bgimage != null)
        {
            setBgImage(aChoiceFromSingleEvent.bgimage);
        }
        //dont flood yo. else { }


        if (aChoiceFromSingleEvent.text != null || aChoiceFromSingleEvent.text !="" )
        {
            setText(aChoiceFromSingleEvent.text);
        }
        else
        {
            Debug.LogWarning("there is no text. does bg image has a text on it ?");
        }
    }

    public void recieveEffectList(List<ASingleEffect> efclist)
    {
        effectlst = new List<ASingleEffect>(efclist);
        Debug.Log("Buton " + gameObject.name + " recieved " + efclist.Count + " effect");
    }

    public void setBgImage(Sprite bgi)
    {
        gameObject.GetComponent<Image>().sprite = bgi;
        //selfimage.sprite = bgi;
        Debug.Log("Buton " + gameObject.name + " recieved new bg image ");
    }

    public void setText(string txt)
    {
        selftext = txt;
        gameObject.GetComponentInChildren<Text>().setTranslationText( txt);
        Debug.Log("Buton " + gameObject.name + " recieved new text ->" + txt);
    }

    void Start()
    {
        Button btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(gotClicked);
    }

    public void gotClicked()
    {
        Debug.Log("Buton sending effect @"+gameObject.name+" to Effect Manager's Effect Queue. EffectManager->recieveEffectList Trigger");
        //Button sending its own effectlist to Effect Manager's Effect Queue. Triggering its recieveEffectList.
        EventManagerGO.GetComponent<EffectManager>().recieveEffectList(effectlst);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
