﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslatableEvent : MonoBehaviour
{
    public string mainTextId;
    public string button1TextId;
    public string button2TextId;
    public string button3TextId;
    public string button4TextId;

    // Start is called before the first frame update
    void Start()
    {
        mainTextId = gameObject.name + "->" + mainTextId;
        mainTextId = gameObject.name + "->" + button1TextId;
        mainTextId = gameObject.name + "->" + button2TextId;
        mainTextId = gameObject.name + "->" + button3TextId;
        mainTextId = gameObject.name + "->" + button4TextId;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
