﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<SpecialSpawnable> SpecialCustomerList;
    [Header("Read Only")]
    public List<SpecialSpawnable> TodaysSpawnable;

    [Header("Main Interval")]
    public float SpawnIntervalSecond;
    [Header("Random Min Max will be add on main interval")]
    [Header("if Main interval 10secs. if minmax 4 secs . Real Randomized interval is between 6-14")]
    public float RandomMinMaxSeconds;

    public bool KeepSpawing;

    public List<GameObject> Slots = new List<GameObject>();
    public List<bool> SlotAvailables = new List<bool>();
    public int SpecialSpawnerInterval;

    public static Spawner Singleton;

    IEnumerator PreLoadCheckerRoutine ()
    {
        /* SpecialSpawnable class
         * 
         * int WhenToSpawn;
         * int MinusPlusTime;
         * int DayToSpawn;
         * GameObject WhoToSpawn;
        */
        while (true)
        {
            //Debug.Log("Trying to special spawn");
            foreach (var tsCust in TodaysSpawnable)
            {
                //Debug.Log(" -> " + tsCust.IsItMyTimeToSpawn() );
                if (tsCust.IsItMyTimeToSpawn())
                {
                    //Debug.Log(" SpawnSpecificSpecial ");
                    SpawnSpecificSpecial(tsCust);
                    break;
                }
            }
            yield return new WaitUntil(() => KeepSpawing == true);
            yield return new WaitForSeconds(SpecialSpawnerInterval);
        }
    }
    public void StartSpawningPreloadedCustomers()
    {
        StartCoroutine(PreLoadCheckerRoutine());
    }
    IEnumerator AutoSpawner()
    {
        float flexTmpTimer;

        while (true)
        {
            if (KeepSpawing)
                Spawn();
            flexTmpTimer = SpawnIntervalSecond + Random.Range(-1.0f * RandomMinMaxSeconds, RandomMinMaxSeconds);
            yield return new WaitForSecondsRealtime(flexTmpTimer);
        }
    }
    public void StartAllSpawners()
    {
        StopAllCoroutines();
        if (SpawnIntervalSecond > 0)
        {
            //Debug.Log(" AutoSpawner Started ");
            StartCoroutine(AutoSpawner());
        }

        //Debug.Log(" TodaysSpawnable.Count "+ TodaysSpawnable.Count);
        if (TodaysSpawnable.Count > 0)
        {
            //Debug.Log(" Preloaded Routine started");
            StartSpawningPreloadedCustomers();
        }

        for (int i = 0; i < SlotAvailables.Count; i++)
        {
            SlotAvailables[i] = true;
        }
    }


    void Start()
    {
        // StartAllSpawners();
    }

    public void SpawnSpecificSpecial( SpecialSpawnable spesificCust )
    {
        Debug.Log("Spawning Special");
        SpawnSpecific(spesificCust.WhoToSpawn);
        //Debug.Log("I FUCKING DELETED IT");
        if ( TodaysSpawnable.Remove(spesificCust) )
        {
            //Debug.Log("I REALLY REALLY  DELETED IT " );
        }

    }
    public void SpawnSpecific ( Customers spesific)
    {
        // get the customer from catalogue
        // Catalogue.Singleton.allTypes[spesific]

        // Add the spawning character to current customers list

        //Instantiate on a target. PARENT is current customers
        // Getting customers from currentCustomers Last ad
        GameObject freshEmptyCustomer = Instantiate( spesific.CustomerBody , CurrentCustomers.Singleton.transform);
        CurrentCustomers.Singleton.AllCusts.Add(freshEmptyCustomer);
        //Find an empty place to spawn
        freshEmptyCustomer.transform.position = getSlotTransform().position;


        //Load Scriptable Object Data into actual customer gameObject
        freshEmptyCustomer.GetComponent<CustomerData>().Data = spesific;


        freshEmptyCustomer.GetComponent<Puppeteer>().OnSpawn();
    }
    public void Spawn()
    {
        // find a random customer Type from Catalogue
        int rnd = Random.Range(0, Catalogue.Singleton.allTypes.Count);
        List<Customers> kiys = Catalogue.Singleton.allTypes.Keys.ToList();


        // -- below area should be done under specificSpawn()

        //add the spawining character to current customers list
        CurrentCustomers.Singleton.AllCusts.Add(Catalogue.Singleton.allTypes[kiys[rnd]]);
        
        //Instantiate on a target. PARENT is current customers
        GameObject freshEmptyCustomer = Instantiate(Catalogue.Singleton.allTypes[kiys[rnd]] , CurrentCustomers.Singleton.transform );

        //Find a place
        freshEmptyCustomer.transform.position = getSlotTransform().position;

        //Load Scriptable Object Data into actual customer gameObject
        freshEmptyCustomer.GetComponent<CustomerData>().Data = kiys[rnd];

        // add movement manager to clone objects
        // Movement manager Depricated.
        // tmp.GetComponent<MovementManager>().enabled = true;

        freshEmptyCustomer.GetComponent<Puppeteer>().OnSpawn();
    }


    // find an available slot
    public Transform  getSlotTransform()
    {
        int rnd = Random.Range(0, Slots.Count);
        Debug.Log("Spawn Slot " + rnd + " returned ");
        return Slots[rnd].transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("c") )
        {
            Spawn();
        }
    }
    private void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }
}

[System.Serializable]
public class SpecialSpawnable
{
    public int WhenToSpawnAsPercent;
    public int MinusPlusTime;
    public int DayToSpawn;
    public Customers WhoToSpawn;

    [Header("Optionals")]
    public string SpawnCondition;
    public int ConditionValue;
    public bool IsItMyTimeToSpawn()
    {
        if (DayCycle.Singleton.TheDay == DayToSpawn && DayCycle.Singleton.DayPercent > WhenToSpawnAsPercent)
        {
            //Default value of Spawncondition on inspector. left empty it will spawn on time without checking
            if (SpawnCondition == "")
            {
                Debug.Log("Time is satisfied. Condition does NOT exist. SPAWNIN");
                return true;
            }
            else if (StatManager.Stats.checkCondition(SpawnCondition, ConditionValue))
            {
                Debug.Log("@spawner. time is satisfied. condition satisfied . SPAWNIN");
                return true;
            }
            else
            {
                Debug.Log("@spawner. time is satisfied. condition is NOT satisfied .");
                return false;
            }
        }
        // this is default check for of spawner.
        return false;
    }

}
