﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallTalk : MonoBehaviour
{
    public float DelayBeforeTextDisappear;
    public bool smallTalkEnded;
    public void WriteFromAPool (string PoolName)
    {
        //find pools get text and pass
        int rnd = Random.Range(0, DialogManager.Singleton.AllPoolComponentsDictionary[PoolName].texts.Count-1);
        string nwtext = DialogManager.Singleton.AllPoolComponentsDictionary[PoolName].texts[rnd];
        StartSmallTalk(nwtext);
    }
    void StartSmallTalk (string writesmtnew)
    {
        gameObject.GetComponentInChildren<TMPwriter>().writeAtext(writesmtnew);
    }

    // typewriter finished writing all all characters
    public void EndTheSmallTalk()
    {
        StartCoroutine(WaitSomeBeforeDestroyingSmallTalk());
    }

    IEnumerator WaitSomeBeforeDestroyingSmallTalk()
    {
        yield return new WaitForSecondsRealtime(DelayBeforeTextDisappear);
        smallTalkEnded = true;
    }

}
