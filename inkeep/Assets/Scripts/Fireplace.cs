﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireplace : MonoBehaviour
{

    public float FireplaceMaxRemainingTime;
    public float remainingTime;
    public float WoodBurnTime;
    public bool currentState;
    public float checkInterval;

    public GameObject WoodAdderButton;
    public GameObject burningS;
    public GameObject extinguisedS;



    private void Start()
    {
        StartCoroutine(remainingTimeRoutine());
        StartCoroutine(HoverButtonRoutine());
    }

    /*
     * Using Child Button Trigger instead of this
     * 
    private void OnMouseDown()
    {
        remainingTime += WoodBurnTime;
        currentState = burningS;
        //adding wood
        Debug.Log("Fireplace Clicked");
    }
    */

    //if fire place hovered . Make button visible
    public void justHovered()
    {
        //adding wood
        WoodAdderButton.GetComponent<Animator>().Play("StationControlsFadeIn");
        buttonVisibilityTimer = 1f;
    }

    float buttonVisibilityTimer;

    // Button visibility cooldown.
    // Stay untill cooldown ends and fadeout
    public IEnumerator HoverButtonRoutine()
    {
        while (true)
        {
            if (buttonVisibilityTimer > 0)
            {
                buttonVisibilityTimer -= Time.deltaTime;
            }
            else
            {
                WoodAdderButton.GetComponent<Animator>().Play("StationControlsFadeOut");
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public void AddSingleWood()
    {
        //triggered by child GO button
        remainingTime += WoodBurnTime;
        currentState = burningS;
        if (remainingTime > FireplaceMaxRemainingTime)
        {
            remainingTime = FireplaceMaxRemainingTime;
        }
        //adding wood
        Debug.Log("AddWood Clicked");
    }

    public IEnumerator remainingTimeRoutine()
    {
        while(true)
        {
            if( remainingTime > 0 && extinguisedS.activeSelf )
            {
                //its burning
                //burning sprite 
                changeSpriteToBurning();
                //opacity sync !!! optimise
                Debug.Log("remainingTime/ FireplaceMaxRemainingTime "+ remainingTime / FireplaceMaxRemainingTime);
            }
            else if (remainingTime < 0.01 && burningS.activeSelf )
            {
                //extinguised
                //change sprite 
                changeSpriteToExtinguised();
            }

            gameObject.transform.Find("4KFireplace_ON").GetComponent<SpriteRenderer>().color = new Color(1,
                                                                                                         1,
                                                                                                         1,
                                                                                                         remainingTime / FireplaceMaxRemainingTime);

            remainingTime -= checkInterval;
            if (remainingTime < 0) remainingTime = 0;
            // pause timer ?? time.deltatime
            yield return new WaitForSeconds(checkInterval);
        }
    }

    public void changeSpriteToBurning()
    {
        JukeBox.Singleton.StartPlayingLoop("FireBurn");
        Debug.Log("Burning");
        burningS.SetActive(true);
        extinguisedS.SetActive(false);
        currentState = true;
    }

    public void changeSpriteToExtinguised()
    {
        JukeBox.Singleton.PausePlayingLoop("FireBurn");
        Debug.Log("Extinguised");
        burningS.SetActive(false);
        extinguisedS.SetActive(true);
        currentState = false;
    }

}
