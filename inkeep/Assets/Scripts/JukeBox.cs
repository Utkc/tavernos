﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JukeBox : MonoBehaviour
{
    public AudioSource mainAudioSoruce;
    public List<SingleClip> TrackList = new List<SingleClip>();

    public void playFromListOS (string trackName)
    {
        foreach (var item in TrackList)
        {
            if (item.AudioName == trackName)
            {
                item.playOS();
                break;
            }
        }
    }

    public void playFromListOS(string trackName, float vol)
    {
        foreach (var item in TrackList)
        {
            if (item.AudioName == trackName)
            {
                item.playOS(vol);
                break;
            }
        }
    }

    public void StartPlayingLoop(string trackName)
    {
        foreach (var item in TrackList)
        {
            if (item.AudioName == trackName)
            {
                item.playLoop();
                break;
            }
        }
    }

    public void PausePlayingLoop(string trackName)
    {
        foreach (var item in TrackList)
        {
            if (item.AudioName == trackName)
            {
                item.pauseLoop();
            }
        }
    }

    public void FadePausePlayingLoop(string trackName)
    {
        foreach (var item in TrackList)
        {
            if (item.AudioName == trackName)
            {
                StartCoroutine( item.audioFadeOutRoutine() );
            }
        }
    }

    void Start()
    {
        // filling audio sources with their loop toogle, volumes and clips 
        foreach (var item in TrackList)
        {

            if (item.dedicatedSource != null)
            {
                item.dedicatedSource.clip = item.Clip;
                item.dedicatedSource.loop = item.isLooping;
                item.dedicatedSource.volume = item.vol;
            }
        }
    }

    public static JukeBox Singleton;
    void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }
}

[System.Serializable]
public class SingleClip
{
    public string AudioName;
    public AudioClip Clip;
    [Header("optional")]
    public bool isLooping;
    public float vol;
    public AudioSource dedicatedSource;

    public void playOS()
    {
        if (dedicatedSource == null)
            JukeBox.Singleton.mainAudioSoruce.PlayOneShot(Clip, 1f);
        else
            dedicatedSource.PlayOneShot(Clip, 1f);
    }
    public void playOS(float vol)
    {
        if (dedicatedSource != null)
            JukeBox.Singleton.mainAudioSoruce.PlayOneShot(Clip, vol);
        else
            dedicatedSource.PlayOneShot(Clip, 1f);
    }

    public void playLoop()
    {
        dedicatedSource.clip = Clip;
        dedicatedSource.loop = isLooping;
        dedicatedSource.volume = vol;
        dedicatedSource.Play();
    }

    public void pauseLoop()
    {
        dedicatedSource.Pause();
    }

    public void pauseLoopFade()
    {
        // This function is moved to audioFadeOutRoutine
        Debug.LogError("Use audioFadeOutRoutine. start coroutine from a monobehaviour");
    }

    public IEnumerator audioFadeOutRoutine()
    {
        float time = 1f;
        while (time > 0)
        {
            time = time - Time.deltaTime;
            dedicatedSource.volume = time;
            yield return new WaitForEndOfFrame();
        }
        dedicatedSource.volume = 0;
        pauseLoop();
    }

}