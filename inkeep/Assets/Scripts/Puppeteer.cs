﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Puppeteer : MonoBehaviour
{
    //This should be prefab. should be populated by Customer->Data component(scriptable object)
    public GameObject orderTemplate;
    public GameObject orderRef;
    public GameObject DialogTemplate;
    /*
     * DIRECTION IS MOVED TO on direction change. triggered by pathmanager...
     * 
     *   float previousXpos;
     *   public bool movingLeft;
    */

    float EatTimer;
    public Customers.OrderType TheOrder;
    public string currentState;
    public Seat reservedSeat;

    public bool west;
    public bool north;
    GameObject activeBody;
    string activeBodyAnimPrefix;

    public GameObject SmallTalkPrefab;
    GameObject dialogRef;
    public float SitDownoffsetX;
    public float SitDownoffsetY;

    SpriteRenderer[] srs;

    public bool DialogStarter;

    public void onDirectionChange(bool left, bool up)
    {
        north = up;
        west = left;

        if (north)
        {
            activeBody = gameObject.transform.Find("bodyPSBnorth").gameObject;
            activeBodyAnimPrefix = "North"; 
        }
        else
        {
            activeBody = gameObject.transform.Find("bodyPSBsouth").gameObject;
            activeBodyAnimPrefix = "South";
        }


        gameObject.transform.Find("bodyPSBnorth").gameObject.SetActive(north);
        gameObject.transform.Find("bodyPSBsouth").gameObject.SetActive(!north);


        if (left && up)
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        else if (left && !up)
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
        else if (!left && up)
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
        else if (!left && !up)
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        else
        {
            Debug.Log( " wtf direction ?! " );
        }

        // switching sprite for up down
    }

    void Update()
    {
        //Depricated direction change. moved to path manager
        /*
        if (previousXpos > gameObject.transform.position.x && movingLeft == true)
            onDirectionChange(!movingLeft);
        else if (previousXpos < gameObject.transform.position.x && movingLeft == false)
            onDirectionChange(!movingLeft);


        previousXpos = gameObject.transform.position.x;
        */
    }

    SpriteRenderer[] srsNorthFace;

    public void OnSpawn()
    {
        EatTimer =  gameObject.GetComponent<CustomerData>().Data.HowSlowIEat;
        srs = GetComponentsInChildren<SpriteRenderer>();
        srsNorthFace = gameObject.transform.Find("bodyPSBnorth").GetComponentsInChildren<SpriteRenderer>();

        foreach (var item in srs)
        {
            item.sortingLayerName = "Walker";
        }

        currentState = "Spawning";

        //if (currentState == "Spawning")
        //{
        if (gameObject.GetComponent<CustomerData>().Data.IsItCounterCustomer())
            GoToBar();
        else
            GoToSeat();
        //}
        //slow fadein goes here
        fadeIn();
    }

    public void GoToBar()
    {
        Debug.Log("going to bar");
        currentState = "Moving To Bar";
        gameObject.GetComponent<PathManager>().NavigateTo(WaypointManager.Singleton.CounterWP.transform.position);
    }

    public void movementEnded()
    {
        if (currentState == "Moving To Seat")
        {
            //activeBody.GetComponent<Animator>().Play(activeBodyAnimPrefix + "Standup"); PLAY IDLE ANIMATION HERE !!!
            SitDown();
        }
        if (currentState == "Moving To Bar")
        {
            //i've reached the bar. start a dialog , an event ...
            StartCoroutine( StartCounterDialog() );

        }
    }

    IEnumerator StartCounterDialog()
    {
        Debug.LogWarning("i came to the bar");
        StartDialog();
        Debug.LogWarning("hello");
        yield return new WaitUntil(() => gameObject.GetComponentInChildren<SingleDialog>().dialogEnded == true);

        Debug.LogWarning("what");
        yield return new WaitForSeconds(1f);
        StartCoroutine( MoveToExit() );
    }

    public bool reached2Exit;

    IEnumerator MoveToExit()
    {
        Debug.LogWarning("wtf");
        GameObject.Destroy(dialogRef.gameObject);
        dialogRef = null;
        currentState = "Moving To Exit";
        gameObject.GetComponent<PathManager>().NavigateTo(ExitWayPointManager.Singleton.TavernExit.transform.position);
        Debug.LogWarning("wtf2");
        yield return new WaitUntil(() => reached2Exit == true);
        Debug.LogWarning("wtf3");
        LeaveTheScene();
    }

    public void GoToSeat()
    {
        // Request Empty Seat
        reservedSeat = InnManager.Singleton.RequestEmptySeat();
        if (reservedSeat == null)
            Debug.LogError(" no reserved seat");
        //Debug.Log("Selected seat : " + reservedSeat.gameObject.name);


        // null pointer exception here !!! @ 200+ customer spawn
        gameObject.GetComponent<PathManager>().NavigateTo(reservedSeat.transform.position);

        // Reserve seat on intent
        // this may be moved to innmanager script
        reservedSeat.setStatus(false);

        currentState = "Moving To Seat";
    }

    public void OrderRecieved(Customers.OrderType ordr)
    {
        Debug.Log(gameObject.GetComponent<CustomerData>().Data.order + " order in the data ");
        Debug.Log("Order Recieved @ puppeteer : " + ordr.ToString());
        TheOrder = ordr;
        currentState = "Eating";
        // Destroy the order pop;
        orderRef.GetComponent<Order>().StopAllCoroutines();
        GameObject.Destroy(orderRef);


        reservedSeat.GetComponentInChildren<Plate>().SpawnPlate(ordr);

        StartCoroutine(EatingCoroutine(EatTimer));
    }

    public void NoServiceGiven()
    {
        //gameObject.GetComponentInChildren<BodySprites>().transform.GetComponent<Animator>().Play("standup");

        //Debug.Log("Reput downed by "+ gameObject.GetComponent<CustomerData>().Data.RepDown);
        //StatManager.Stats.ConsumeItemBy("Reput", gameObject.GetComponent<CustomerData>().Data.RepDown);
        StartCoroutine( BeforeStandingUp() );
    }

    public void StartDialog()
    {
        Debug.Log("dialog started");
        dialogRef = Instantiate(DialogTemplate, gameObject.transform, true);
        dialogRef.GetComponent<SingleDialog>().recieveTextList(gameObject.GetComponent<CustomerData>().Data.DialogTexts);
        dialogRef.GetComponent<SingleDialog>().manualStart();
        dialogRef.GetComponent<SingleDialog>().EventIdToStart = gameObject.GetComponent<CustomerData>().Data.EventId ;
        dialogRef.transform.position = gameObject.transform.position;
        //new dialog position is determined by the animator.
        dialogRef.transform.position = new Vector3(dialogRef.transform.position.x, dialogRef.transform.position.y, dialogRef.transform.position.z);
        
    }

    public IEnumerator BeforeStandingUp()
    {

        if (gameObject.GetComponent<CustomerData>().Data.DialogTexts.Count != 0)
        {
            Debug.Log("Dialog Before Standing Up ");
            StartDialog();
            yield return new WaitUntil(() => gameObject.GetComponentInChildren<SingleDialog>().dialogEnded == true);
            GameObject.Destroy(gameObject.GetComponentInChildren<SingleDialog>().gameObject);
            //dialog ended wait some before leaving
            //implement wait untill event ending
            yield return new WaitForSecondsRealtime(3f);
        }
        SittingEndedMoveToExit();
    }

    public void SittingEndedMoveToExit()
    {
        reservedSeat.GetComponentInChildren<Plate>().DisappearPlate();
        InnManager.Singleton.RelaseSeat(reservedSeat);
        currentState = "Leaving";
        //gameObject.GetComponent<Animator>().Play("mainchar");
        activeBody.GetComponent<Animator>().Play(activeBodyAnimPrefix + "Standup");
        fadeOut();

        //do this after customer gotoexit()
        //LeaveTheScene();
    }


    IEnumerator EatingCoroutine(float Timer)
    {
        if (gameObject.GetComponent<CustomerData>().Data.SmallTalkPoolName != "" )
        {
            Debug.Log("INSTANTIATED");
            StartCoroutine(SmallTalkRoutine());
        }
        else
        {
            Debug.Log("NOTTTT INSTANTIATED");
        }

        //GameObject.Destroy(orderRef);
        Debug.Log("Eating Started will eat for "+EatTimer);
        for (float i = Timer; i > 0; i -= Time.deltaTime)
        {
            yield return new WaitForEndOfFrame();
        }
        Debug.Log("Done Eating");
        StatManager.Stats.IncreaseItemBy("Reput", gameObject.GetComponent<CustomerData>().Data.RepUp);
        Debug.Log("Reput increased by " + gameObject.GetComponent<CustomerData>().Data.RepUp);
        StartCoroutine(BeforeStandingUp());
    }


    IEnumerator SmallTalkRoutine()
    {
        Instantiate(SmallTalkPrefab,gameObject.transform);
        string STpoolName = gameObject.GetComponent<CustomerData>().Data.SmallTalkPoolName;
        gameObject.GetComponentInChildren<SmallTalk>().WriteFromAPool(STpoolName);
        yield return new WaitUntil(() => gameObject.GetComponentInChildren<SmallTalk>().smallTalkEnded == true);
        GameObject.Destroy(gameObject.GetComponentInChildren<SmallTalk>().gameObject);
    }

    public void PopOrder()
    {
        //Debug.Log("Im ordering");
        orderRef = Instantiate(orderTemplate, transform);
        orderRef.transform.position = transform.position + new Vector3(0f,2.2f,0f);
        orderRef.GetComponent<Order>();
    }

    // Use this to wait fade in
    public void FadeInEnded()
    {
        // gameObject.GetComponent<MovementManager>().GoToSomeWhere();
        // gameObject.GetComponent<PathManager>().NavigateTo(WaypointManager.Singleton.wayPoints[4].transform.position );

        /*
        if (currentState == "Spawning")
        {
            if (gameObject.GetComponent<CustomerData>().Data.IsItCounterCustomer() )
                GoToBar();
            else
                GoToSeat();
        }
        */

        if (currentState == "On Seat")
        {
            //Debug.Log("I should order");
            PopOrder();
        }
    }

    public void SitDown()
    {
        currentState = "On Seat";
        //fade out !!! fade in on seat
        fadeOut();
    }


    public void FadeOutEnded()
    {
        if(currentState == "On Seat")
        {
            //get west north position according to seat
            onDirectionChange(!reservedSeat.westFacing, reservedSeat.northFacing);


            // Mendetory to switch North Body sort layers for mask interactions
            foreach (var item in srs)
            {
                item.sortingLayerName = "Default";
            }
            foreach (var item in srsNorthFace)
            {
                item.sortingLayerName = "NorthFacingBody";
            }


            //teleport to seat and fade back in
            gameObject.transform.position = reservedSeat.transform.position + new Vector3(SitDownoffsetX, SitDownoffsetY, reservedSeat.transform.position.z);
            activeBody.transform.GetComponent<Animator>().Play( activeBodyAnimPrefix + "Sitting");
            fadeIn();
        }
        // All hail for the reusabe code
        if(currentState == "Leaving")
        {
            // Mendetory to switch North Body sort layers for mask interactions
            foreach (var item in srsNorthFace)
            {
                item.sortingLayerName = "Walker";
            }
            foreach (var item in srs)
            {
                item.sortingLayerName = "Walker";
            }


            gameObject.transform.position = gameObject.GetComponent<PathManagerExit>().FindClosestWaypoint(gameObject.transform.position).transform.position;
            fadeIn();
            
            gameObject.GetComponent<PathManagerExit>().NavigateTo( ExitWayPointManager.Singleton.ExitDoor.transform.position );
            activeBody.GetComponent<Animator>().Play(activeBodyAnimPrefix + "Walk");
        }
    }

    public void fadeOut()
    {
        StartCoroutine(FadeAllChildImage(true));
    }
    public void fadeIn()
    {
        StartCoroutine(FadeAllChildImage(false));
    }

    IEnumerator SlowFadeIn(float time)
    {
        for (float i = 0; i <= time; i += Time.deltaTime)
        {
            foreach (SpriteRenderer eachSR in srs)
            {
                eachSR.material.color = new Color(1, 1, 1, EaseInSineD(0, 1, i));
            }

            yield return null;
        }
    }

    //fades all child spriteRenderer
    IEnumerator FadeAllChildImage(bool fadeAway)
    {
        //srs is all child sprite renderers.
        // initialised at onspawn . wait timer and pop order should not get involved
        
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                foreach (SpriteRenderer eachSR in srs)
                {
                    eachSR.material.color = new Color(1, 1, 1, i );
                }
                yield return null;
            }
        FadeOutEnded();
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                foreach (SpriteRenderer eachSR in srs)
                {
                    eachSR.material.color = new Color(1, 1, 1, EaseInSineD(0, 1, i) );
                }
                
               yield return null;
            }
        FadeInEnded();
        }
    }

    public static float EaseInOutCirc(float start, float end, float value)
    {
        value /= .5f;
        end -= start;
        if (value < 1) return -end * 0.5f * (Mathf.Sqrt(1 - value * value) - 1) + start;
        value -= 2;
        return end * 0.5f * (Mathf.Sqrt(1 - value * value) + 1) + start;
    }

    public static float EaseInSineD(float start, float end, float value)
    {
        return (end - start) * 0.5f * Mathf.PI * Mathf.Sin(0.5f * Mathf.PI * value);
    }


    public void LeaveTheScene()
    {
        Debug.Log("Leaving The Place");
        StartCoroutine(LeaveTheSceneRoutine());
    }

    IEnumerator LeaveTheSceneRoutine()
    {
        Debug.Log(gameObject.name + " Has Left The Scene1");

        //reservedSeat.isEmpty = true;
        for (float i = 1; i >= 0; i -= Time.deltaTime)
        {
            foreach (SpriteRenderer eachSR in srs)
            {
                eachSR.material.color = new Color(1, 1, 1, i);
            }
            yield return new WaitForEndOfFrame();
        }
        Debug.Log(gameObject.name + " Has Left The Scene2");
        GameObject.Destroy(this.gameObject);
    }

}
