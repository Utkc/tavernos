﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitTimer : MonoBehaviour
{
    public float time;
    Coroutine c;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("timer started");
        StartWaiting();
    }

    public void StartWaiting()
    {
        //timer Start
        Debug.Log("Waiting");

        // signal order for start waiting
        transform.GetComponentInParent<Order>().SetupOrderSprites( transform.GetComponentInParent<CustomerData>().Data.order );
        if (c == null)
        {
            c = StartCoroutine(WaitRoutine(gameObject.GetComponentInParent<CustomerData>().Data.HowLongShouldIWaitForService));
        }
        else
        {
            StopWaiting();
            c = StartCoroutine(WaitRoutine(gameObject.GetComponentInParent<CustomerData>().Data.HowLongShouldIWaitForService));
        }
    }
    public void StopWaiting()
    {
        StopCoroutine(c);
    }

    IEnumerator WaitRoutine(float t)
    {
        for (float i = t; i >= 0; i -= Time.deltaTime)
        {
            gameObject.transform.localScale = new Vector3( i/t , transform.localScale.y , transform.localScale.z );
            yield return new WaitForEndOfFrame();
        }
        WaitEndedNoService();
    }

    public void WaitEndedNoService()
    {
        Debug.Log("Order Wait Ended. no service. -rep ...");
        StatManager.Stats.ConsumeItemBy("Reput", gameObject.GetComponentInParent<CustomerData>().Data.RepDown);
        gameObject.GetComponentInParent<Order>().endOrder();
        gameObject.GetComponentInParent<Puppeteer>().NoServiceGiven();
        
        //Destroy order box
        //GameObject.Destroy(gameObject.transform.parent.GetComponent<Order>().gameObject);
        // Destroy self. timer bar
        //GameObject.Destroy(gameObject);
    }

}
