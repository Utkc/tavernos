﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTracker : MonoBehaviour
{
    public Camera myCam;

    void MouseInfoPrint()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        foreach (var hit in hits)
        {
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.transform.GetComponent<StationManager>() != null)
                {
                    //mouse is on a station
                    hit.collider.gameObject.transform.GetComponent<StationManager>().ReInitControllersFadeOutTimer();
                }
            }
        }

    }
    void MouseInfoPrint2()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        foreach (var hit in hits)
        {
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.transform.GetComponent<StationManager>() != null)
                {
                    //mouse is on a station
                    hit.collider.gameObject.transform.GetComponent<StationManager>().ReInitControllersFadeOutTimer();
                }
            }
        }

    }
    void FirePlaceTracker()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        foreach (var hit in hits)
        {
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.transform.GetComponent<Fireplace>() != null)
                {
                    //mouse is on a station
                    hit.collider.gameObject.transform.GetComponent<Fireplace>().justHovered();
                }
            }
        }

    }

    private void Start()
    {

    }

    void Update()
    {
        MouseInfoPrint();
        MouseInfoPrint2();
        FirePlaceTracker();
    }

}
