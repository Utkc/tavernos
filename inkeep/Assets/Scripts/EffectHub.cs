﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectHub : MonoBehaviour
{
    //public Image selfimage;
    public string selftext;
    public List<ASingleEffect> effectlst;

   

    // Start is called before the first frame update

    public void recieveEffectList(List<ASingleEffect> efclist)
    {
        effectlst = new List<ASingleEffect>(efclist);
        Debug.Log("Buton "+gameObject.name+" recieved "+efclist.Count+" effect");
    }

    public void setBgImage(Sprite bgi)
    {
        gameObject.GetComponent<Image>().sprite = bgi;
        //selfimage.sprite = bgi;
        Debug.Log("Buton " + gameObject.name + " recieved new bg image ");
    }

    public void setText (string txt)
    {
        selftext = txt;
        Debug.Log("Buton " + gameObject.name + " recieved new text ");
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
