﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventScreenUiManager : MonoBehaviour
{
    public GameObject TheDialogStartedThisEvent;
    public GameObject EventManagerObject;
    public string asd;

    public List<Button> buts;
    public Image img;
    public Text txtui;

    public void setupNewEvent(GameObject evntobj)
    {
        setUiElements(evntobj);
    }

    public void setUiElements (GameObject gmobj)
    {
        // gmobj is the GAME OBJECT that has single event component which holds everything that a screen canvas can show at a time.
        // get the game object that holds the data of an single event . extract data in the <single event> Component
        Debug.Log("setting ui elements with the id of -> "+ gmobj.GetComponent<SingleEvent>().EventId );


        //change translatable ids


        //delete this set each button text thing
        //for (int i = 0; i < gmobj.GetComponent<SingleEvent>().PossibleChoices.Count ; i++)
        //{
        //    buts[i].GetComponentInChildren<Text>().text = gmobj.GetComponent<SingleEvent>().PossibleChoices[i].text;
        //}

        //set bg image of screen Canvas
        img.sprite = gmobj.GetComponent<SingleEvent>().EimageSprite;
        // set the main text on canvas
        txtui.setTranslationText(gmobj.GetComponent<SingleEvent>().Etext);

        // each possible choice has a text for button, bg image for button, effect that button is going to have, and where that buttons leads
        List<SingleChoice> sclist = gmobj.GetComponent<SingleEvent>().PossibleChoices;
        for (int i = 0; i < sclist.Count; i++)
        {
            buts[i].GetComponent<ButtonManager>().setButtonWithSingleChoice(sclist[i]);

        /* FOLLOWING PART ARE DONE INTERNALLY @setButtonWithSingleEvent
         
            if ( sclist[i].EffectList.Count != 0)
            {
                buts[i].GetComponent<ButtonManager>().recieveEffectList(sclist[i].EffectList);
            }
            if (sclist[i].bgimage != null)
                buts[i].GetComponent<ButtonManager>().setBgImage(sclist[i].bgimage);

            buts[i].GetComponent<ButtonManager>().setText(sclist[i].text);
        */
        }


    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
