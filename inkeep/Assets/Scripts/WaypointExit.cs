﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointExit : MonoBehaviour
{
	public List<WaypointExit> neighbors;

	public WaypointExit previous
	{
		get;
		set;
	}

	public float distance
	{
		get;
		set;
	}

	void OnDrawGizmos()
	{
		if (neighbors == null)
			return;
		Gizmos.color = new Color(0f, 0f, 0.9f);
		foreach (var neighbor in neighbors)
		{
			if (neighbor != null)
				Gizmos.DrawLine(transform.position, neighbor.transform.position);
		}
	}

}
