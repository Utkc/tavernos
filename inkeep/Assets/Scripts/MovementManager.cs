﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour
{
    // multiple waypoints for to be followed.
    // List.Count should have the closest target
    public List<GameObject> targetWayPoints;
    public string status;
    public void GoToSomeWhere()
    {
        Debug.Log("spawned");
        recieveWayPoints();
    }

    // Get list of positions and feed them into targetwaypoint.
    public void recieveWayPoints(List<GameObject> targets)
    {
        targetWayPoints = new List<GameObject>(targets);
    }

    // Go straight to bar
    public void recieveWayPoints()
    {
        targetWayPoints = new List<GameObject>( WaypointManager.Singleton.wayPoints );
        targetWayPoints.Reverse();
        StartCoroutine( moveRoutine() );

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKeyDown("x"))
        {
            recieveWayPoints();
            GoToSomeWhere();
        }
    }

    IEnumerator moveRoutine ()
    {
        while (targetWayPoints.Count > 0)
        {
            moveToEnd();
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    float step = 0.01f;
    // Update is called once per frame
    public void moveToEnd()
    {
        moveTo( targetWayPoints[targetWayPoints.Count - 1] );
        if ( Vector2.Distance( targetWayPoints[targetWayPoints.Count-1].transform.position , gameObject.transform.position ) < 0.5f )
        {
            targetWayPoints.RemoveAt(targetWayPoints.Count - 1);
            Debug.Log("Removed target waypoint @" + (targetWayPoints.Count - 1) );
            // if you are on closest target. remove the latest targetWayPoint[Count] . to continue moving the next target.
        }
    }

    void moveTo(GameObject trgt)
    {
        if(gameObject.transform.position != trgt.transform.position)
        {
            Vector3 self = gameObject.transform.position;
            Vector3 stepTowards = Vector3.MoveTowards(gameObject.transform.position, trgt.transform.position, step);
            gameObject.transform.position = stepTowards;

        }

    }
}
