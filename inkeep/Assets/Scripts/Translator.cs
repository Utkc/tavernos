﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Translator : MonoBehaviour
{
    //text GO will have translatable component. it will record itself to this dictionary.
    //Dictionary holds every visible text object.
    public Dictionary<string, GameObject> Translatables = new Dictionary<string, GameObject>();
    public Dictionary<string, string> ActiveLanguage = new Dictionary<string, string>();
    // if something goes wrong. these values will be used as index or and values
    // Text objects already has a text value on it (and cant be used as ids)
    public Dictionary<string, string> DefaultLanguage = new Dictionary<string, string>();

    public static Translator Singleton;

    public void exportDefaultLang() 
    {

    }
    
    public void translateAll()
    {
        // after active language is populated. place all texts into 
        foreach (var TranslatablesRecord in Translatables)
        {
            TranslatablesRecord.Value.GetComponent<Translatable>().translate();
        }
    }


    void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }

}

// use this when changeing visible text objects.
// use .text english default string, find active language entry corresponds to active language.
// default language is the language on th editor
public static class TextExtension
{
    public static void setTranslationText(this Text t, string str)
    {
        t.text = str;
    }
}

