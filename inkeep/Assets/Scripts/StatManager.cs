﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using UnityEngine;

public class StatManager : MonoBehaviour
{
    public Dictionary<string, float> relations = new Dictionary<string,float>();
    public Dictionary<string, float> relationMultipliers = new Dictionary<string, float>();
    public int gold;
    public Dictionary<string, float> items = new Dictionary<string, float>();
    public Dictionary<string, int> ConditionTable = new Dictionary<string, int>();

    //debug stuff
    public List<Stat> ReadOuts = new List<Stat>();
    public static StatManager Stats;

    public bool checkCondition(string cnd,int val)
    {
        if (ConditionTable.ContainsKey(cnd))
        {
            if(ConditionTable[cnd]==val)
                return true;
            else
            {
                Debug.Log("Condition is valid values does not match");
                return false;
            }
        }
        else
        {
            Debug.Log("The condition being checked is not in the table");
            return false;
        }
            
    }

    public bool addCondition(string cnd,int val)
    {
        if(ConditionTable.ContainsKey(cnd))
        {
            Debug.LogError("Duplicated Condition is being entered");
            return false;
        }
        else
        {
            ConditionTable.Add(cnd,val);
            return true;
        }

    }

    public void IncreaseItem(string item)
    {
        if (items.ContainsKey(item))
        {
            items[item] += 1;
        }
        else
        {
            Debug.LogError("there is no such item" + item);
        }

    }
    public void IncreaseItemBy(string item,int amnt)
    {
        if (item == "Reput")
            Debug.Log("INCRE Statmanager -> " + item + " : " + amnt);

        if (items.ContainsKey(item))
        {
            items[item] += amnt;
        }
        else
        {
            Debug.LogError("there is no such item called "+item);
        }

    }

    public bool isConsumable(string item)
    {
        if (items.ContainsKey(item) && items[item] > 0)
        {
            return true;
        }
        else if (!items.ContainsKey(item))
        {
            Debug.Log("Stat Manager -> item doesnt exist");
            return false;
        }
        else if (!(items[item] > 0))
        {
            Debug.Log("Stat Manager -> not enough item");
            return false;
        }
        else
        {
            Debug.LogError("!!! need fix");
            return false;
        }
    }
    public bool isConsumableBy(string item,int by)
    {
        if (item == "Reput")
            Debug.Log("CONSUM Statmanager -> " + item + " : " + by);

        if (items.ContainsKey(item) && items[item] >= by)
        {
            return true;
        }
        else if (!items.ContainsKey(item))
        {
            Debug.Log("Stat Manager -> item doesnt exist");
            return false;
        }
        else if ( !(items[item] > by))
        {
            Debug.Log("Stat Manager -> not enough item");
            return false;
        }
        else
        {
            Debug.LogError("!!! need fix");
            return false;
        }
    }
    public bool ConsumeItem(string item)
    {
        if (isConsumable(item))
        {
            Debug.Log("previous item value "+ item + items[item]);
            items[item] -= 1;
            Debug.Log("current item value " + item + items[item]);
            return true;
        }
        else
        {
            Debug.Log("Cant consume item :" + item);
            return false;
        }

    }
    public bool ConsumeItemBy(string item,int by)
    {
        if (isConsumable(item))
        {
            items[item] -= by;
            return true;
        }
        else
        {
            Debug.Log("Cant consume item :" + item);
            return false;
        }

    }

    public void factionRelationModify (string factionName,float val)
    {
        relations[factionName] = relations[factionName] + (val* relationMultipliers[factionName]) ;
    }
    
    public void factionValueSet (string factionName, float val)
    {
        relations[factionName] = val;
        relationMultipliers[factionName] = val;
    }

    public void factionWideMultiplierSet (string factionName, float val)
    {
        relationMultipliers [factionName] = val;
    }


    // Start is called before the first frame update
    void Start()
    {
        // move it to indexes manager perhaps? dunno.
        // generate dictionary from indexes inspector screen

        //fill up the dictionaries using list (!!! GUI friedly workaround)
        foreach (var item in gameObject.GetComponent<Indexes>().factionNamesString)
        {
            relations.Add(item, 0f);
            relationMultipliers.Add(item, 0f);
        }

        foreach (var item in gameObject.GetComponent<Indexes>().itemNames )
        {
            items.Add(item, 15f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        ReadOuts.Clear();
        /*
        foreach (var item in relations)
        {
            Stat stt = new Stat();
            stt.sName = item.Key;
            stt.sValue = item.Value;
            ReadOuts.Add(stt);
        }
        foreach (var item in relationMultipliers)
        {
            Stat stt = new Stat();
            stt.sName = item.Key;
            stt.sValue = item.Value;
            ReadOuts.Add(stt);
        }
        */
        foreach (var item in items)
        {
            Stat stt = new Stat();
            stt.sName = item.Key;
            stt.sValue = item.Value;
            ReadOuts.Add(stt);
        }
    }

    //singleton for inspector friendly class. !avoid static class
    //Singleton acess
    void Awake()
    {
        if (Stats != null)
            GameObject.Destroy(Stats);
        else
            Stats = this;

        DontDestroyOnLoad(this);
    }

}

[Serializable]
public class Stat
{
    public string sName;
    public float sValue;
}