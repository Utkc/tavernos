﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testinmultitypes : MonoBehaviour
{
    List<basetype> obj = new List<basetype>();
    // Start is called before the first frame update
    void Start()
    {
        /*
        type1 obj1 = new type1("txt1");
        basetype obj2 = new type1("txt2");
        type1 obj3 = new basetype() as type1;

        obj.Add(obj1);
        obj.Add(obj2);
        obj.Add(obj3);

        Debug.Log("asd1 " + obj[0].GetType() + " " + obj1);
        Debug.Log("asd2 " + obj[1].GetType() + " " + obj2);
        */
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}

public class basetype
{

}

public class type2:basetype
{
    int type2int;

    public type2(int var)
    {
        this.type2int = var+5;
    }
}

public class type1:basetype
{
    string type1str;
    public type1(string var)
    {
        this.type1str = var +"3";
    }
}