﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TMPwriter : MonoBehaviour
{

    public TextMeshProUGUI TMchild;
    int charCount;
    int visibleCharacterCount;


    void Start()
    {
        TMchild = gameObject.GetComponent<TextMeshProUGUI>();
        StartCoroutine(manualStart());
    }

    public void writeAtext (string newText)
    {
        StopAllCoroutines();
        TMchild.text = newText;
        StartCoroutine(manualStart());
    }

    public IEnumerator manualStart()
    {
        visibleCharacterCount = 0;
        charCount = TMchild.text.Length;

        while (true)
        {
            visibleCharacterCount++;
            //move to shownexttext
            TMchild.maxVisibleCharacters = visibleCharacterCount;
            yield return new WaitForSecondsRealtime(0.1f);
            yield return new WaitUntil(() => visibleCharacterCount < charCount);
            if(visibleCharacterCount >= charCount)
                gameObject.GetComponentInParent<SmallTalk>().EndTheSmallTalk();
        }
    }

}
