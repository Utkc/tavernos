﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
using System.IO;
using UnityEngine.UI;

public class FileManager : MonoBehaviour
{
    public List<TextAsset> LanguageFiles;
    public TextAsset DefaultLanguage;
    Dictionary<string, string> tmp;
    // Start is called before the first frame update
    public void exportLanguage()
    {
        Debug.Log("Export Language Started");
        string path = "Assets/Resources/exportedLanguage.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine("asd");
        foreach (var item in Translator.Singleton.Translatables)
        {
            writer.WriteLine(item.Key+"#"+item.Value.GetComponent<Text>().text);
        }

        foreach (var item in EventManager.Singleton.EventsDictionary)
        {
            writer.WriteLine(item.Value.name + "#" + item.Value.GetComponent<SingleEvent>().Etext);
            int counter = 0;
            foreach (var ite in item.Value.GetComponent<SingleEvent>().PossibleChoices)
            {
                writer.WriteLine(item.Value.name+"Button"+counter+"#"+ite.text );
                counter++;
            }
        }

        writer.Close();
        Debug.Log("Export Language Ended");
    }
    public void switchLanguage(int i)
    {
        //StreamReader reader = new StreamReader(Application.dataPath + "/Languages/Lang1.txt");
        //Debug.Log(Application.dataPath+" data path ");
        //Debug.Log("hello wo " + reader.ReadToEnd());
        tmp = Translator.Singleton.ActiveLanguage;
        StringReader reader = new StringReader(LanguageFiles[i].text);
        string line = "";
        int counter = 0;
        string[] elem = { "", "" };

        tmp.Clear();

        while ((line = reader.ReadLine()) != null)
        {
            elem = line.Split('#');
            tmp.Add(elem[0], elem[1]);
            //Debug.Log(elem[0] +"  "+ elem[1] );
            counter++;
        }
        Translator.Singleton.translateAll();

        /*
        Debug.Log(counter + " items loaded from the LANGUAGE text asset");
        Debug.Log(tmp.Count + " items loaded from the LANGUAGE text asset");
        Debug.Log("active language count " + Translator.Singleton.ActiveLanguage.Count);
        */
    }

    void Start()
    {
        switchLanguage(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void loadDefaultLanguage()
    {
        StringReader reader = new StringReader(DefaultLanguage.text);
        string line = "";
        int counter = 0;
        string[] elem = { "", "" };

        Translator.Singleton.DefaultLanguage.Clear();

        while ((line = reader.ReadLine()) != null)
        {
            elem = line.Split('#');
            Translator.Singleton.DefaultLanguage.Add(elem[0], elem[1]);
            //Debug.Log(elem[0] +"  "+ elem[1] );
            counter++;
        }
        Debug.Log("Language has " + Translator.Singleton.DefaultLanguage.Count + " entries in it and loaded ");
    }
}
public class HandleTextFile
{
    [MenuItem("Tools/Write file")]
    static void WriteString()
    {
        string path = "Assets/Resources/test.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine("Test");
        writer.Close();

        //Re-import the file to update the reference in the editor
        AssetDatabase.ImportAsset(path);
        TextAsset asset = (TextAsset)Resources.Load("test");

        //Print the text from the file
        Debug.Log(asset.text);
    }

    [MenuItem("Tools/Read file")]
    static void ReadString()
    {
        string path = "Assets/Resources/test.txt";

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        Debug.Log(reader.ReadToEnd());
        reader.Close();
    }

}