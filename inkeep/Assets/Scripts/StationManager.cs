﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationManager : MonoBehaviour
{
    public string stationType;
    public List<GameObject> Storables;


    public float cookInterval;
    public float burnInterval;

    public float MaxBurnCapacity;
    public float WoodBurnTime;
    public float FoodCookTime;

    public GameObject FoodAdder;
    public GameObject WoodAdder;
    public GameObject GlowObject;

    public Sprite NotCookingImage;
    public Sprite CookingImage;

    public Sprite NotBurningImage;
    public Sprite BurningImage;

    public GameObject CookerChildObject;
    public GameObject BurnerChildObject;


    public float ControllerFadeOutRemainingTime;
    Coroutine coru;

    public GameObject CookTimerBar;
    public GameObject BurnTimerBar;

    public float localCookTimer;
    public float localBurnTimer;

    public bool isCooking;
    public bool isBurning;
    public bool isStorageFull()
    {
        foreach (var SingleStorable in Storables)
        {
            if (!SingleStorable.activeSelf)
            {
                return false;
            }
        }
        return true;
    }

    public bool isStorageEmpty()
    {
        foreach (var SingleStorable in Storables)
        {
            if (SingleStorable.activeSelf)
            {
                return false;
            }
        }
        return true;
    }


    public int findEmptyStorable()
    {
        for (int i = 0; i < Storables.Count; i++)
        {
            if (!Storables[i].activeSelf)
                return i;
        }
        return -1;
    }
    public bool consumeStorable()
    {
        if (!isStorageEmpty())
        {
            foreach (var item in Storables)
            {
                if(item.activeSelf)
                {
                    item.SetActive(false);
                    return true;
                }
            }
        }
        else
        {
            Debug.LogError("Cant Consume Storable Logic error");
            return false;
        }
        Debug.LogError("Cant Consume Storable Logic error");
        return false;
    }

    public bool storeNewFood()
    {
        if (!isStorageFull())
        {
            StatManager.Stats.IncreaseItem(stationType);
            Storables[findEmptyStorable()].SetActive(true);
            return true;
        }
        else
        {
            Debug.LogWarning("Storage Full ... Cooked Food Discarded by Station Manager");
            return false;
        }
    }

    public int storedAmount()
    {
        int res = 0 ;
        foreach (var item in Storables)
        {
            if (item.activeSelf)
                res++;
        }
        return res;
    }



    public void ReInitControllersFadeOutTimer()
    {
        MouseEntered();
        ControllerFadeOutRemainingTime = 1f;
        if (coru != null)
        {
            StopCoroutine(coru);
            coru = StartCoroutine(FadeOutWaitMore());
        }
        else
            coru = StartCoroutine(FadeOutWaitMore());
    }

    IEnumerator FadeOutWaitMore()
    {
        while (ControllerFadeOutRemainingTime > 0)
        {
            ControllerFadeOutRemainingTime -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        MouseLeft();
    }

    public void MouseLeft()
    {
        // FadeOut Timer
        // gameObject.GetComponentInChildren<StationTimer>().gameObject.GetComponent<Animator>().Play("StationTimerFadeOut");

        FoodAdder.GetComponent<Animator>().Play("StationControlsFadeOut");
        WoodAdder.GetComponent<Animator>().Play("StationControlsFadeOut");
        GlowObject.GetComponent<Animator>().Play("StationControlsFadeOut");
    }

    public void MouseEntered()
    {
        FoodAdder.GetComponent<Animator>().Play("StationControlsFadeIn");
        WoodAdder.GetComponent<Animator>().Play("StationControlsFadeIn");
        GlowObject.GetComponent<Animator>().Play("StationControlsFadeIn");
    }

    public void UpdateCookingImage()
    {
        if (isCooking)
        {
            CookerChildObject.GetComponent<SpriteRenderer>().sprite = CookingImage;
        }
        else
        {
            CookerChildObject.GetComponent<SpriteRenderer>().sprite = NotCookingImage;
        }
    }

    public void UpdateBurningImage()
    {
        if (isBurning)
        {
            BurnerChildObject.GetComponent<SpriteRenderer>().sprite = BurningImage;
        }
        else
        {
            BurnerChildObject.GetComponent<SpriteRenderer>().sprite = NotBurningImage;
        }
    }



    void Start()
    {
        StartCoroutine(CookingTimer());
        StartCoroutine(BurningTimer());
    }

    public IEnumerator CookingTimer()
    {
        // pause scale mek ??
        while (true)
        {
            if (localCookTimer < 1)
            {
                //New Food is Cooked and Ready
                if (!isStorageFull() && isCooking)
                {
                    Debug.Log("Food Store");
                    storeNewFood();
                }
                else if (isStorageFull() && isCooking)
                {
                    Debug.LogWarning("Food Wasted");
                    // cooked food wasted
                }
                else
                {
                    // station is idle
                }
                isCooking = false;
                JukeBox.Singleton.PausePlayingLoop("Cooking" + stationType);
            }
            else
            {
                //still cooking

                JukeBox.Singleton.StartPlayingLoop("Cooking" + stationType);
                isCooking = true;
                localCookTimer -= cookInterval;
            }

            CookTimerBar.transform.localScale = new Vector3( localCookTimer / FoodCookTime, 1, 0);
            UpdateBurningImage();
            // cooking may be paused
            yield return new WaitUntil(() => isBurning == true);
            // cooking is continued
            yield return new WaitForSeconds(cookInterval);
        }
    }

    public IEnumerator BurningTimer()
    {
        //pause scale mek ??
        while (true)
        {
            if (localBurnTimer < burnInterval)
            {
                isBurning = false;
            }
            else
            {
                isBurning = true;
                localBurnTimer -= burnInterval;
            }
            //opacity sync !!! optimise
            //!!! 
            gameObject.transform.Find("BurnerChildObject").GetComponent<SpriteRenderer>().color = new Color(gameObject.transform.Find("BurnerChildObject").GetComponent<SpriteRenderer>().color.r, gameObject.transform.Find("BurnerChildObject").GetComponent<SpriteRenderer>().color.g, gameObject.transform.Find("BurnerChildObject").GetComponent<SpriteRenderer>().color.b, localBurnTimer / MaxBurnCapacity);
            //!!! 
            BurnTimerBar.transform.localScale = new Vector3(localBurnTimer / MaxBurnCapacity ,1,0);
            UpdateCookingImage();
            yield return new WaitForSeconds(burnInterval);
        }
    }


    public void AddWood()
    {
        localBurnTimer += WoodBurnTime;
        if (localBurnTimer > MaxBurnCapacity)
            localBurnTimer = MaxBurnCapacity;
    }


    public void AddFood()
    {

        //new food can only be added if not cooking
        if (localCookTimer < cookInterval)
            localCookTimer += FoodCookTime;
        JukeBox.Singleton.playFromListOS("Cook"+stationType);
        CookTimerBar.transform.localScale = new Vector3(localCookTimer / FoodCookTime, 1, 0);
    }

    void Update()
    {

    }

}
