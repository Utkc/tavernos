﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DayCycle : MonoBehaviour
{
    public static DayCycle Singleton;
    public int TheDay { get; set; }

    //total seconds;
    public int HowManySecondsADayHas;
    public int SecondsPassed;

    //every 5 seconds 
    [Header(" inner variables ")]
    public int TickInterval;
    public int TickPassed;

    public int MaxTick;
    public float DayPercent;

    public void StartTheDay (int day)
    {
        TheDay = day;
        MaxTick = HowManySecondsADayHas / TickInterval;
        Debug.Log(" Day:" + day + " begins");

        List<SpecialSpawnable> tmpList = new List<SpecialSpawnable>();
        foreach (var acust in Spawner.Singleton.SpecialCustomerList)
        {
            if (acust.DayToSpawn == TheDay)
            {
                tmpList.Add(acust);
            }
        }
        Spawner.Singleton.TodaysSpawnable = new List<SpecialSpawnable>(tmpList);
        Debug.Log(" Todays Special Spawner Count " + Spawner.Singleton.TodaysSpawnable.Count );


        StartCoroutine(DayPasssingRoutine());
        //use coroutine to start spawners to add delay

        //StartCoroutine(StartAllSpawnersWithDelayRoutine() ); multiple execution
    }

    IEnumerator StartAllSpawnersWithDelayRoutine()
    {
        //wait 2 seconds and start all routines
        yield return new WaitForSeconds(2f);
        Spawner.Singleton.StartAllSpawners();
    }

    IEnumerator DayPasssingRoutine()
    {
        int i = 0;
        while (SecondsPassed < HowManySecondsADayHas )
        {
            SecondsPassed += TickInterval;
            TickPassed = i;
            DayPercent = (TickPassed * TickInterval * 1.0f / HowManySecondsADayHas) * 100.0f;

            i++;
            //ClockTicked(i);
            yield return new WaitForSeconds(TickInterval);
        }
        SecondsPassed = 0;
        DayEnded();
        // TheDay *1.0f / TickInterval*1.0f;
    }

    public void DayEnded()
    {
        TheDay++;
        StartTheDay(TheDay);
        StartCoroutine(StartAllSpawnersWithDelayRoutine());
    }

    public void ClockTicked(int i)
    {
        DayPercent = (TickPassed*TickInterval * 1.0f / HowManySecondsADayHas) *100.0f ;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartTheDay(0);
        StartCoroutine(StartAllSpawnersWithDelayRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }

}
