﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UI;
using UnityEngine;

public class EffectManager : MonoBehaviour
{
    public GameObject statHoldingGameO;
    //this is a child object. no need until multiple custom function holders. public GameObject customEffectHoldingGameObject;
    public ASingleEffect efc;
    public List<ASingleEffect> effectQueue;

    public void recieveEffectList(List<ASingleEffect> efl)
    {
        this.effectQueue = efl;
        Debug.Log("EffectManager recieved Effects");

        if (effectQueue.Count > 0)
        {
            somei = 0;
            Debug.Log("Iterating effect queue");
            StartCoroutine(effectRellease(0.1f));
        }
    }

    int somei=0;
    IEnumerator effectRellease(float delay)
    {
        while (effectQueue.Count>0)
        {
            Debug.Log("Effect in queue executed index at " + somei);
            executeAnEffect(effectQueue[0]);
            effectQueue.RemoveAt(0);
            somei++;
            yield return new WaitForSeconds(delay);
        }
    }

    public void executeAnEffect(ASingleEffect efc)
    {
        //is conditionsatisfied. it will return true if coçndition is null
        if (isConditionSatisfied(efc))
        {
            if (!isConditionable(efc))
                Debug.Log("No Condition for the effect");
            else if (isConditionSatisfied(efc))
                Debug.Log("Condition Satisfied for " + efc.efcondition.availableDataType + " " + efc.efcondition.availableData + " " + efc.efcondition.comparator + " " + efc.efcondition.targetValue);
            else
                Debug.Log("Its a mystery how this debug line appeared");

            //Fin effects type and execute related procedure 
            if (isEffectCustom(efc))
            {
                (efc.effectSelf as CustomEFCS).ExecuteEffect();
                Debug.Log("custom effect executed. custom effect function name : " + (efc.effectSelf as CustomEFCS).customEffectFunctionName);
                //gameObject.GetComponentInChildren<customEffects>().customEffectInvoker( (efc.effectSelf as CustomEffectTemplate).customEffectFunctionName );
            }
            else if (isEffectFaction(efc))
            {
                //AFactionEffect efac = null;
                //efac.ExecuteEffect();
                (efc.effectSelf as FactionEffectCS).ExecuteEffect();
                Debug.Log("faction effect executed : " + (efc.effectSelf as FactionEffectCS).factionName + " " + (efc.effectSelf as FactionEffectCS).effectType + " " + (efc.effectSelf as FactionEffectCS).value);
            }
            else if (isEffectItem(efc))
            {
                //AItemEffect efit = null;
                //efit.ExecuteEffect(efit.ItemName , efit.ItemValueSet);
                (efc.effectSelf as FactionEffectCS).ExecuteEffect();
            }
            else if (isEffectJump(efc))
            {
                //AItemEffect efit = null;
                //efit.ExecuteEffect(efit.ItemName , efit.ItemValueSet);
                (efc.effectSelf as EventChangeCS).ExecuteEffect();
            }
            else if (isEffectHistory(efc))
            {
                //AItemEffect efit = null;
                //efit.ExecuteEffect(efit.ItemName , efit.ItemValueSet);
                (efc.effectSelf as HistoryGenerator).ExecuteEffect();
            }
            else
            {
                Debug.Log("Something is wrong with this effect type");
            }
        }
        else
        {
            Debug.Log("condition is not satisfied for "+ efc.efcondition.availableDataType + " " + efc.efcondition.availableData + " " + efc.efcondition.comparator+" "+efc.efcondition.targetValue );
        }
    }

    public bool isConditionSatisfied(ASingleEffect efc)
    {
        if (!isConditionable(efc))
            return true;

        Debug.Log("checking condition " + efc.efcondition.availableDataType + " " + efc.efcondition.availableData + " " + efc.efcondition.comparator + " " + efc.efcondition.targetValue);

        if (efc.efcondition != null)
        {
            float tocheck = -1.23456f;
            float target = float.Parse(efc.efcondition.targetValue);
            string compi = efc.efcondition.comparator; 
            if (efc.efcondition.availableDataType == "item")
            {
                tocheck = StatManager.Stats.items[efc.efcondition.availableData];
            }
            else if (efc.efcondition.availableDataType == "faction")
            {
                tocheck = StatManager.Stats.relations[efc.efcondition.availableData];
            }
            else
            {
                tocheck = -9.9999f;
                Debug.LogError("There is something wrong with the Effect->Condition->DataType index:" +efc.effectSelf.name );
            }

            if(compi == "==" && tocheck == target)
                return true;
            else if (compi == "!=" && tocheck != target)
                return true;
            else if (compi == "<=" && tocheck <= target)
                return true;
            else if (compi == ">=" && tocheck >= target)
                return true;
            else
            {
                Debug.Log("The condition " + efc.efcondition.availableDataType + " " + tocheck + " " + compi + " " + target + " FAILED");
                return false;
            }
        }
        else
        {
            Debug.LogError("Condition is empty but we are checking it for some reason");
            return false;
        }
    }

    public bool isConditionable(ASingleEffect efc) 
    {
        if (efc.efcondition == null)
        {
            Debug.Log("condition null");
            return false;
        }
        if (efc.efcondition.availableData != null && efc.efcondition.availableData != "")
            return true;

        return false;
    }

    // rework needed at effect types and execution of scriptableobject->execute() design.

    public bool isEffectCustom(ASingleEffect anef)
    {
        if (anef.effectSelf.GetType().ToString() == "CustomEventTemplate")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool isEffectJump(ASingleEffect anef)
    {
        if (anef.effectSelf.GetType().ToString() == "EventChangeTemplate")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool isEffectHistory(ASingleEffect anef)
    {
        if (anef.effectSelf.GetType().ToString() == "HistoryGenerator")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool isEffectFaction(ASingleEffect anef)
    {
        if (anef.effectSelf.GetType().ToString() == "ItemEffectTemplate")
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    public bool isEffectItem(ASingleEffect anef)
    {
        if (anef.effectSelf.GetType().ToString() == "FactionEffect")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
