﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogAnimHolder : MonoBehaviour
{
    public void triggerStopDialog() 
    {
        gameObject.GetComponentInParent<SingleDialog>().DialogDiscarded();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
