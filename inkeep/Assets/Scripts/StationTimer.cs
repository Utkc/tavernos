﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationTimer : MonoBehaviour
{
    public float ClockMaxSeconds;
    public float RemainingTime;
    public float DecayRate;
    public float WoodFillPercent;
    public bool HaveFuel;



    Coroutine c;
    // Start is called before the first frame update
    
    void Start()
    {
        //initialize a outof fuel station
        RemainingTime = 0f;
        //StationWorks();
        gameObject.transform.localScale = new Vector3( 0 , transform.localScale.y, transform.localScale.z);
    }

    public void ReActivateTheStation()
    {
        //add fuel relative to the clockmaxseconds + 20%
        RemainingTime = RemainingTime + ((ClockMaxSeconds/100)*WoodFillPercent);
        //if remaining time passed .Clock max seconds. equalise...
        if (ClockMaxSeconds < RemainingTime)
            RemainingTime = ClockMaxSeconds;

        StationWorks();
    }

    public void StationWorks()
    {
        if (c == null)
            c = StartCoroutine(StationWorkRoutine());
        else
        {
            StopCoroutine(c);
            c = StartCoroutine( StationWorkRoutine() );
        }
    }

    IEnumerator StationWorkRoutine()
    {
        //Debug.Log(GetComponentInParent<Station>().StationType + " reactivated , fuel timer reset");
        gameObject.transform.parent.GetComponent<Station>().StationActive = true;
        while (RemainingTime>0)
        {
            HaveFuel = true;
            RemainingTime -= Time.deltaTime * DecayRate;
            gameObject.transform.localScale = new Vector3( Mathf.Lerp(0f, 1f,  RemainingTime/ClockMaxSeconds) , transform.localScale.y , transform.localScale.z);
            
            yield return new WaitForEndOfFrame();
        }
        HaveFuel = false;
        GetComponentInParent<Station>().DeactivateTheStation();
        GetComponentInParent<Station>().StationActive = false;
        //Debug.Log(GetComponentInParent<Station>().StationType + " station is out of fuel.");

    }
}
