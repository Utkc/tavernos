﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogPool : MonoBehaviour
{
    public string poolName;
    public List<string> texts;
    [HideInInspector]
    public List<int> usageData;

    void Start()
    {
        //Record yourself to pool manager   
        DialogManager.Singleton.AllPoolComponents.Add(this);

        DialogManager.Singleton.AllPoolComponentsDictionary.Add( poolName ,this);
    }

}
