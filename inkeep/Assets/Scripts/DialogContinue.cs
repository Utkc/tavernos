﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogContinue : MonoBehaviour
{

    private void OnMouseUp()
    {
        gameObject.GetComponentInParent<SingleDialog>().showNextText();
    }
}
