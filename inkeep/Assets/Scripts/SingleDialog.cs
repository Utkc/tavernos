﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class SingleDialog : MonoBehaviour
{
    public string dialogID;
    public string EventIdToStart;

    public List<string> Texts;
    public int ActiveTextIndex = 0;

    public bool dialogEnded;

    public void recieveTextList (List<string> ls)
    {
        Texts = new List<string>(ls);
        ActiveTextIndex = 0;
    }

    //might be transfared to animator

    public float endDialogWaitTime;
    public float dialogInteractionTimer;
    float dialogITcurrentValue;

    public IEnumerator waitForDialogInteraction()
    {
        dialogITcurrentValue = dialogInteractionTimer;
        while (dialogITcurrentValue>0)
        {

            dialogITcurrentValue -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    //might be transfared to animator

    public bool EventInteractionEnded = false;
    public IEnumerator DialogEnded()
    {
        Debug.Log("Dialog ended 1 ");
        //send this dialog to eventmanager so end of event can trigger leave bar routine
        GameObject.Find("Event Screen").GetComponent<EventScreenUiManager>().TheDialogStartedThisEvent = gameObject;
        Debug.Log("Dialog ended 11");
        GameObject.Find("EventManager_H_N").GetComponent<EventManager>().goToEvent(EventIdToStart);
        //dialogs start an event . wait untill event interaction ends

        Debug.Log("Dialog ended 2");
        yield return new WaitUntil(() => EventInteractionEnded == true);

        Debug.Log("Dialog ended 3");
        yield return new WaitForSeconds(1f);
        dialogEnded = true;
    }

    public void DialogDiscarded()
    {
        dialogEnded = true;
        //dont call any event
    }

    public void showNextText()
    {
        gameObject.transform.GetChild(0).GetComponent<Animator>().Play("DilalogWait",0,0f);

        if (Texts.Count == ActiveTextIndex)
        {
            showContinBut();
        }
        else
        {
            hideContinBut();
            TMchild.text = Texts[ActiveTextIndex];
            visibleCharacterCount = 0;
            charCount = Texts[ActiveTextIndex].Length;
            ActiveTextIndex++;
        }

    }

    public TextMeshProUGUI TMchild;
    public int charCount;
    public int visibleCharacterCount;
    public GameObject DialogEndSp;
    public GameObject BGsp;
    public GameObject ContinBut;

    public void manualStart()
    {
        StartCoroutine( manualStartRoutine() );
    }

    public IEnumerator manualStartRoutine()
    {
        Debug.Log("MANUAL Start");
        showNextText();

        while (true)
        {
            visibleCharacterCount++;
            //move to shownexttext
            TMchild.maxVisibleCharacters = visibleCharacterCount;
            yield return new WaitForSecondsRealtime(0.1f);
            yield return new WaitUntil(() => visibleCharacterCount < charCount);
        }
    }


    /*
    // Start is called before the first frame update
    IEnumerator Start()
    {
        Debug.Log("normal Start");
        //TMchild = transform.GetComponentInChildren<TextMeshProUGUI>();
        showNextText();

        while (true)
        {
            visibleCharacterCount++;
            //move to shownexttext
            charCount = Texts[ActiveTextIndex].Length;
            TMchild.maxVisibleCharacters = visibleCharacterCount;
            yield return new WaitForSecondsRealtime(0.1f);
            yield return new WaitUntil(() => visibleCharacterCount < charCount );
        }

    }
    */

    void showContinBut()
    {
        DialogEndSp.SetActive(true);
        TMchild.gameObject.SetActive(false);
        BGsp.gameObject.SetActive(false);
        ContinBut.gameObject.SetActive(false);
    }
    void hideContinBut()
    {
        DialogEndSp.SetActive(false);
        TMchild.gameObject.SetActive(true);
        BGsp.gameObject.SetActive(true);
        ContinBut.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
