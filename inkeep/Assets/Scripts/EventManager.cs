﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public GameObject EventCanvas; 
    public Dictionary<string,GameObject> EventsDictionary = new Dictionary<string, GameObject>();

    public static EventManager Singleton;
    public GameObject cnvsHolder;
    void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }

    public void goToEvent (string id)
    {
        Debug.Log("event go2");
        beforeGOTO();
        EventCanvas.GetComponent<EventScreenUiManager>().setupNewEvent(getSingleEvent(id));
    }

    public void goToEvent(GameObject eventGO) 
    {
        beforeGOTO();
        EventCanvas.GetComponent<EventScreenUiManager>().setupNewEvent(eventGO);
    }

    public void beforeGOTO()
    {
            Debug.Log("initshow");
        if (cnvsHolder.GetComponent<CanvasObjectHolder>().VisibilityState == false)
        {
            cnvsHolder.GetComponent<CanvasObjectHolder>().initShow();
        }
    }

    public GameObject getSingleEvent(string eid)
    {
        if (EventsDictionary.ContainsKey(eid))
        {
            return EventsDictionary[eid];
        }
        else
        {
            Debug.Log("get single event cant find a gameobject(s.event) called " + eid);
            return null;
        }
    }

    public bool isEventPresented (GameObject gobj)
    {
        //check if the event exist in the dictionary
        if (EventsDictionary.ContainsValue(gobj))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool setSingleEvent(string eid,GameObject gobj)
    {
        if ( !EventsDictionary.ContainsKey(eid))
        {
            EventsDictionary[eid] = gobj;
            return true;
        }
        else
        {
            Debug.Log(" <color=red> Event insertion failed due to : Duplicate Tkey @eventManager->eventsDictionary </color> ");
            return false;
        }
    }


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void showNewEvent(string eid)
    {
        //setup the visual objects under event screen canvas ( public GameObject EventCanvas )
        GameObject sevt;
        sevt = EventsDictionary[eid];

        // get the game object from the dictionary ... send it to ui manager.
        EventCanvas.GetComponent<EventScreenUiManager>().setUiElements(EventsDictionary[eid]);
    }

    void printEvents()
    {
        IDictionaryEnumerator myEnumerator = EventsDictionary.GetEnumerator();
        while (myEnumerator.MoveNext() )
        {
            Debug.Log("Print event "+ myEnumerator.Current.ToString() );
        }
    }

}
