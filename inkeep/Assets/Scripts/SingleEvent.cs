﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class SingleEvent : MonoBehaviour
{
    public string EventId;
    //public string Etype;
    
   // public Image Eimage;
    public Sprite EimageSprite;
    public string Etext;

    [Tooltip("both event itself and choices can have seperate effects")]
    public List<SingleChoice> PossibleChoices = new List<SingleChoice>();
    [Tooltip("fill either , items , factions or custom")]
    public List<ASingleEffect> EffectList = new List<ASingleEffect>();


    // Start is called before the first frame update
    void Start()
    {
        //import sprite data to image object
        //Eimage.sprite = EimageSprite; 

        //if name is not specified
        if (EventId == null)
        {
            EventId = gameObject.name;
        }

        // enlist the game object to the dictionary in event manager.
        gameObject.transform.parent.GetComponent<EventManager>().EventsDictionary.Add(EventId,this.gameObject);

        //Debug.Log(PossibleChoices[0].EffectList[0].effectSelf.GetType() );
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void testcustomEffect()
    {
        //cse.Execute();
    }
}

[Serializable]
public class SingleChoice
{
    public string text;
    public Sprite bgimage;

    public List<ASingleEffect> EffectList = new List<ASingleEffect>();
}

[Serializable]
public class ASingleEffect 
{
     
    public ConditionCS efcondition;
    public ScriptableObject effectSelf;
}


/*

[Serializable]
public class IAnyEffect {

}




[Serializable]
public class AFactionEffect 
{
    public int FactionEffectTurnCount;
    // Faction A , Faction B, Throne Faction, Not throne faction
    public string FactionName;

    // use strings for nullibility default 0f
    public string FactionModify = null;

    //default 1f
    public string FactionWideMultiplier = null ;

    // sets the value directly 
    public string FactionValueSet = null;

    public void ExecuteEffect()
    {
        if (FactionModify != null)
            StatManager.Stats.relations[FactionName] += float.Parse(FactionModify);
        if(FactionValueSet != null)
            StatManager.Stats.relations[FactionName] = float.Parse(FactionModify);
        if (FactionWideMultiplier != null)
            StatManager.Stats.relationMultipliers[FactionName] = float.Parse(FactionModify);
    }
}

[Serializable]
public class AItemEffect 
{
    // --------------------- for item ---------------------
    // gold ,asd1 ,asd2
    public string ItemName;

    //Relative Modification ... default 0
    public float ItemValueModify;
    //change the multiplier ... default 1
    public float ItemGlobalMultiplier;
    // directly set the value
    public float ItemValueSet;

    public void ExecuteEffect()
    {
        Debug.Log("Wrong execute effect started item effect. this one is default interface ... Use the one with parameters.");
    }

    public void ExecuteEffect(string iname, float ival)
    {
        StatManager.Stats.relationMultipliers[iname] = ival;
    }
}

[Serializable]
public class ACustomEffect 
{
    // --------------------- for custom magic ---------------------
    //change multiplier for a period of time ... default 0
    public string CustomEffectFuntionName;

    [Tooltip("leave this empty. this might be used there lots of funky effects")]
    public GameObject CustomEffectFuntionHolder;

    public void CustomEffectFunctionExecute()
    {
        CustomEffectFuntionHolder.GetComponent<customEffects>().Invoke(CustomEffectFuntionName, 0);
    }

    public void ExecuteEffect()
    {
        CustomEffectFunctionExecute();
    }
}*/