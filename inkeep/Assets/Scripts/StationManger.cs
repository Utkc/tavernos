﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationManger : MonoBehaviour
{
    float timer;

    public IEnumerator generictimer(float delay,float totalTime , bool countdown = false)
    {
        float timerval = totalTime;
        while ( delay > 0 )
        {
            delay -= Time.deltaTime;
        }

        if (countdown)
        {
            while (timerval > 0)
            {
                timerval -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            while (timerval < 0)
            {
                timerval -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(generictimer(0,2) );
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
