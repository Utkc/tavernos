﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

public class CanvasObjectHolder : MonoBehaviour
{
    public float hideYpos;
    public float showYpos;
    public float SlideSpeed;
    float currentYpos;
    public bool VisibilityState;

    // Start is called before the first frame update
    void Start()
    {
        currentYpos = transform.position.y;
        VisibilityState = false;
    }

    public void showCanvas()
    {
        gameObject.GetComponent<Animator>().Play("EventScreenSHOW");
        /*
            while (currentYpos > showYpos)
        {
            currentYpos = currentYpos + (SlideSpeed * -1);
            transform.localPosition = new Vector3(transform.localPosition.x, currentYpos, transform.localPosition.z);
            yield return new WaitForFixedUpdate();
        }
        VisibilityState = true;
        */

    }

    public void hideCanvas()
    {
        gameObject.GetComponent<Animator>().Play("EventScreenHIDE");

        // if event didnt call the event canvas this will cause issues.
        // after event interaction The Dialog startedthis event should be null.
        //event screen ui manager reusablity issue !!!
        if(gameObject.GetComponentInParent<EventScreenUiManager>().TheDialogStartedThisEvent != null)
        {
            gameObject.GetComponentInParent<EventScreenUiManager>().TheDialogStartedThisEvent.GetComponent<SingleDialog>().EventInteractionEnded = true;
            gameObject.GetComponentInParent<EventScreenUiManager>().TheDialogStartedThisEvent = null;
        }

        /*
        while (currentYpos < hideYpos)
        {
            currentYpos = currentYpos + SlideSpeed ;
            transform.localPosition = new Vector3(transform.localPosition.x, currentYpos, transform.localPosition.z);
            yield return new WaitForFixedUpdate();
        }
        */
        VisibilityState = false;
    }

    public void initShow()
    {
        StopAllCoroutines();
        showCanvas();
    }
    public void initHide()
    {
        StopAllCoroutines();
        hideCanvas();
    }


}
