﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
    public List<DialogPool> AllPoolComponents = new List<DialogPool>();

    public Dictionary<string,DialogPool> AllPoolComponentsDictionary = new Dictionary<string,DialogPool>();

    public static DialogManager Singleton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }
}
