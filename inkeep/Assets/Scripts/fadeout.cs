﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeout : MonoBehaviour
{
    

    //fades all child spriteRenderer
    IEnumerator FadeAllChildImage(bool fadeAway,GameObject obj)
    {
        Component[] srs;
        srs = GetComponentsInChildren<SpriteRenderer>();
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                foreach (SpriteRenderer eachSR in srs)
                {
                    eachSR.material.color = new Color(1, 1, 1, i);
                }

                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            Debug.Log("asd nooo");
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                gameObject.GetComponentInChildren<SpriteRenderer>().material.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown("z"))
        {
            Debug.Log("qqq");
            StartCoroutine(FadeAllChildImage(true,gameObject));
        }
        if (Input.GetKeyDown("x"))
        {
            Debug.Log("www");
            StartCoroutine(FadeAllChildImage(false,gameObject));
        }
        */
    }

}
