﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManagerExit : MonoBehaviour
{
	public float walkSpeed = 1.0f;

	private Stack<Vector3> currentPath;
	private Vector3 currentWaypointPosition;
	private float moveTimeTotal;
	private float moveTimeCurrent;

	public bool left;
	public bool up;

	// !!!  a trigger need . to handle arrived on target mechanics.its on stop()


	// http://www.trickyfast.com/2017/09/21/building-a-waypoint-pathing-system-in-unity/

	//set direction for sprite flips
	//call puppeteer.ondirectionchange
	//triggered when new waypoint is set for destination
	public void DirectionChange(bool left, bool up)
	{
		gameObject.GetComponent<Puppeteer>().onDirectionChange(left, up);
	}

	//any point is acceptable. cloesest WayPoint will be the target
	public void NavigateTo(Vector3 destination)
	{
		currentPath = new Stack<Vector3>();
		var currentNode = FindClosestWaypoint(transform.position);
		var endNode = FindClosestWaypoint(destination);
		if (currentNode == null || endNode == null || currentNode == endNode)
			return;
		var openList = new SortedList<float, WaypointExit>();
		var closedList = new List<WaypointExit>();
		openList.Add(0, currentNode);
		currentNode.previous = null;
		currentNode.distance = 0f;
		while (openList.Count > 0)
		{
			currentNode = openList.Values[0];
			openList.RemoveAt(0);
			var dist = currentNode.distance;
			closedList.Add(currentNode);
			if (currentNode == endNode)
			{
				break;
			}
			foreach (var neighbor in currentNode.neighbors)
			{
				if (closedList.Contains(neighbor) || openList.ContainsValue(neighbor))
					continue;
				neighbor.previous = currentNode;
				neighbor.distance = dist + (neighbor.transform.position - currentNode.transform.position).magnitude;
				var distanceToTarget = (neighbor.transform.position - endNode.transform.position).magnitude;
				openList.Add(neighbor.distance + distanceToTarget, neighbor);
			}
		}
		if (currentNode == endNode)
		{
			while (currentNode.previous != null)
			{
				currentPath.Push(currentNode.transform.position);
				currentNode = currentNode.previous;
			}
			currentPath.Push(transform.position);
		}
	}

	public void Stop()
	{
		currentPath = null;
		moveTimeTotal = 0;
		moveTimeCurrent = 0;
		//gameObject.GetComponent<Puppeteer>().movementEnded();

		if (gameObject.GetComponent<Puppeteer>().currentState == "Moving To Exit")
		{
			gameObject.GetComponent<Puppeteer>().reached2Exit = true;
		}
		else
		{
			//local function movement ended decides if cust is reaching for bar or a seat
			gameObject.GetComponent<Puppeteer>().movementEnded();
		}
	}

	void Update()
	{

		if (currentPath != null && currentPath.Count > 0)
		{
			if (moveTimeCurrent < moveTimeTotal)
			{
				moveTimeCurrent += Time.deltaTime;
				if (moveTimeCurrent > moveTimeTotal)
					moveTimeCurrent = moveTimeTotal;
				transform.position = Vector3.Lerp(currentWaypointPosition, currentPath.Peek(), moveTimeCurrent / moveTimeTotal);
			}
			else
			{
				currentWaypointPosition = currentPath.Pop();
				if (currentPath.Count == 0)
				{
					Stop();
					gameObject.GetComponent<Puppeteer>().LeaveTheScene();
				}
				else
				{
					moveTimeCurrent = 0;
					moveTimeTotal = (currentWaypointPosition - currentPath.Peek()).magnitude / walkSpeed;

					///////////////////////////////////////////////////////////////////////SETTING DIRECTION START
					/////Debug.Log("New target WP is set");
					/////Debug.Log("My   point :" + transform.position);
					/////Debug.Log("Trgt point :" + currentPath.Peek());

					// direction change.set up sprites and flips
					if (transform.position.x > currentPath.Peek().x && transform.position.y > currentPath.Peek().y)
					{
						DirectionChange(false, false);
					}
					else if (transform.position.x > currentPath.Peek().x && transform.position.y < currentPath.Peek().y)
					{
						DirectionChange(false, true);
					}
					else if (transform.position.x < currentPath.Peek().x && transform.position.y > currentPath.Peek().y)
					{
						DirectionChange(true, false);
					}
					else if (transform.position.x < currentPath.Peek().x && transform.position.y < currentPath.Peek().y)
					{
						DirectionChange(true, true);
					}
					/*
					Debug.Log("flags1 " + (transform.position.x > currentPath.Peek().x && transform.position.y > currentPath.Peek().y));
					Debug.Log("flags2 " + (transform.position.x > currentPath.Peek().x && transform.position.y < currentPath.Peek().y));
					Debug.Log("flags3 " + (transform.position.x < currentPath.Peek().x && transform.position.y > currentPath.Peek().y));
					Debug.Log("flags4 " + (transform.position.x < currentPath.Peek().x && transform.position.y < currentPath.Peek().y));
					*/
					///////////////////////////////////////////////////////////////////////SETTING DIRECTION END
				}
			}
		}
	}

	public WaypointExit FindClosestWaypoint(Vector3 target)
	{
		GameObject closest = null;
		float closestDist = Mathf.Infinity;
		foreach (var waypoint in ExitWayPointManager.Singleton.wayPoints)
		{
			var dist = (waypoint.transform.position - target).magnitude;
			if (dist < closestDist)
			{
				closest = waypoint;
				closestDist = dist;
			}
		}
		//Debug.Log("traversed all waypoints and returned :" + closest.transform.gameObject.name);
		if (closest != null)
		{
			return closest.GetComponent<WaypointExit>();
		}
		else
		{
			Debug.Log("cant find closest target");
		}
		return null;
	}

}
