﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{

	// http://www.trickyfast.com/2017/09/21/building-a-waypoint-pathing-system-in-unity/
	public List<Waypoint> neighbors;

	public Waypoint previous
	{
		get;
		set;
	}

	public float distance
	{
		get;
		set;
	}

	void OnDrawGizmos()
	{
		if (neighbors == null)
			return;
		Gizmos.color = new Color(0f, 0f, 0f);
		foreach (var neighbor in neighbors)
		{
			if (neighbor != null)
				Gizmos.DrawLine(transform.position, neighbor.transform.position);
		}
	}


}
