﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnManager : MonoBehaviour
{
    public Seat DebugSeat;
    public static InnManager Singleton;
    public List<Seat> AllSeats;
    public List<Seat> EmptySeats;
    public List<Seat> OccupiedSeats;

    public void RelaseSeat(Seat unusedSeat)
    {
        OccupiedSeats.Remove(unusedSeat);
        EmptySeats.Add(unusedSeat);
    }

    public Seat RequestEmptySeat()
    {
        if (EmptySeats.Count > 0)
        {
            //reserve a random empty seat
            int rnd = Random.Range(0, EmptySeats.Count);
            //save the picked seat
            Seat anEmptySeat = EmptySeats[rnd];
            //put seat to the occupied seats
            OccupiedSeats.Add(anEmptySeat);
            //remove seat from empty seats list since its geting reserved now
            EmptySeats.Remove(anEmptySeat);
            //return seat to the puppetter onSPAWN
            return anEmptySeat;
        }
        else
        {
            Debug.Log("NO EMPTY SEAT");
            return DebugSeat;
        }


    }

    private void Awake()
    {
        if (Singleton != null)
            GameObject.Destroy(Singleton);
        else
            Singleton = this;

        DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        // collect all seats and register them to all seats
        Seat[] seatComps =  gameObject.GetComponentsInChildren<Seat>();
        foreach (var item in seatComps)
        {
            AllSeats.Add(item);
            EmptySeats.Add(item);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Refresh Seat Lists status
        /* 
         * This brokes some stuff due to update execution times
         * 
        EmptySeats.Clear();
        OccupiedSeats.Clear();
        foreach (var item in AllSeats)
        {
            if (item.isSeatEmpty())
            {
                EmptySeats.Add(item);
            }
            else
            {
                OccupiedSeats.Add(item);
            }
        }
        */

    }
}
