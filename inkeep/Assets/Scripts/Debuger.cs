﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuger : MonoBehaviour
{
    public string function;
    public string compName;
    public float delay;
    // Start is called before the first frame update
    void Start()
    { 
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void initInvoke()
    {
        Invoke("invoker", delay);
    }
    void invoker()
    {
        (gameObject.GetComponent(compName) as MonoBehaviour).Invoke(function,0f);
        Debug.Log("Invoked ->"+compName+" @"+function+" under:"+gameObject.name);
    }

}
