﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class Translatable : MonoBehaviour
{
    public string referenceId;

    public void translate()
    {
        //change your own text component
        Debug.Log("Attempting text switch with "+referenceId);
        
        if (Translator.Singleton.ActiveLanguage.ContainsKey(referenceId))
        {
            Debug.Log(" singleton " + Translator.Singleton.ActiveLanguage[referenceId] + "value recieved ");
            gameObject.GetComponent<Text>().text = Translator.Singleton.ActiveLanguage[referenceId];
        }
        else
        {
            Debug.LogError("'" + referenceId + "' cannot find the translation in the singleton dictionary");
            gameObject.GetComponent<Text>().text = Translator.Singleton.DefaultLanguage[referenceId];
            Debug.Log("Dictionary Entries;");
            foreach (var item in Translator.Singleton.ActiveLanguage)
            {
                Debug.Log(item.Key+"->"+item.Value);
            }
        }
    }

    public void setTextWithId (string refid)
    {
        this.referenceId = refid;
        translate();
    }

    void Start()
    {
        //record self to the translatables index
        Translator.Singleton.Translatables.Add(referenceId, gameObject);
        Debug.Log("Translatable recorded with "+referenceId);
    }


}
