﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Empty Custom Effect", menuName = "Effects/Custom")]
public class CustomEFCS : ScriptableObject
{
    public string customEffectFunctionName;
    // Start is called before the first frame update


    public void ExecuteEffect()
    {
        customEffects.Itself.customEffectInvoker(customEffectFunctionName);
    }
}
