﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item Effect", menuName = "Effects/Item")]
public class ItemEffectCS : ScriptableObject
{
    public string itemName;
    [Header("Optional")]
    public Sprite itemicon;

    // modify or set
    [Header("pick one 'modify' or 'set' modify ")]
    public string effectType;
    public float value;

    public void ExecuteEffect()
    {
        if (effectType == "modify")
            StatManager.Stats.items[itemName] += value; // .relations[factionName] += value;
        if (effectType == "set")
            StatManager.Stats.items[itemName] = value;
        //if (FactionWideMultiplier != null)
        //    StatManager.Stats.relationMultipliers[FactionName] = float.Parse(FactionModify);
    }
}
