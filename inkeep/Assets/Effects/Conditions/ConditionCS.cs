﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Empty Condition", menuName = "Condition")]
public class ConditionCS : ScriptableObject
{
    [Header(" item  OR  faction ")]
    public string availableDataType;
    [Header(" itemNAME  OR  factionNAME ")]
    public string availableData;
    [Header(" '<' '>' '==' '<=' '>=' '!=' ")]
    public string comparator;
    public string targetValue;
}
