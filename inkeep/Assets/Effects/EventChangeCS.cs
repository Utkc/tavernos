﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Event Change Effect", menuName = "Effects/JumpToEvent")]
public class EventChangeCS : ScriptableObject
{
    GameObject eventMan;
    //[Header("Drag and Drop GO (must be event)")]
    //GameObject EventToJump;
    [Header("Or Just Write The Event Id (Not Game ObjectName... Event id in the inspetor)")]
    public string Eventid;

    void Awake()
    {
        eventMan = GameObject.Find("EventManager_H_N");
    }

    public void ExecuteEffect()
    {
        eventMan.GetComponent<EventManager>().goToEvent(Eventid);
        /*
        if (Eventid != null && Eventid != "")
            eventMan.GetComponent<EventManager>().goToEvent(Eventid);
        else if (EventToJump != null)
            eventMan.GetComponent<EventManager>().goToEvent(Eventid);
        */
    }
}
