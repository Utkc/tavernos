﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Empty Faction Effect", menuName = "Effects/Faction")]
public class FactionEffectCS : ScriptableObject
{
    public string factionName;
    [Header("'modify' or 'set'")]
    public string effectType;
    public float value;

    [Header("Optional")]
    public Sprite factionEffectIcon;
    [Header("Optional")]
    public string effectDescription;

    public void ExecuteEffect()
    {
        if (effectType == "modify")
            StatManager.Stats.relations[factionName] += value;
        if (effectType == "set")
            StatManager.Stats.relations[factionName] = value;
        //if (FactionWideMultiplier != null)
        //    StatManager.Stats.relationMultipliers[FactionName] = float.Parse(FactionModify);
    }
}
