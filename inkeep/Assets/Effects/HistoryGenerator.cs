﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "History Effect", menuName = "Effects/History")]
public class HistoryGenerator : ScriptableObject
{
    public string HistoryEntry;
    public int Value;

    public void ExecuteEffect()
    {
        if (HistoryEntry == "")
        {
            Debug.LogError("History entry is left empty.Can't execute");
        }
        else
        {
            StatManager.Stats.addCondition(HistoryEntry, Value);
            Debug.Log("New history event is added to the list : "+HistoryEntry +" "+Value);
        }
    }

}
