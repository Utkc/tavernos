﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cooking : MonoBehaviour
{
    public GameObject cookTimer;
    public float StationSpeed;

    public int CookAmount;
    public bool isSpotEmpty = true;
    public float remainingTime = 0;
    public float Cooktime;


    private void Start()
    {
        isSpotEmpty = true;
    }

    public void StartCooking()
    {
        //start cooking routine with this stations own cook timer.
        StartCoroutine( CookRoutine ( Cooktime ) );
        gameObject.GetComponent<SpriteRenderer>().color = new Color(0.99f, 0.1f, 0.1f, 0.99f);
    }

    IEnumerator CookRoutine(float t)
    {
        isSpotEmpty = false;
        // add food visual

        for (float k = t; k >= 0; k -= Time.deltaTime * StationSpeed)
        {
            //cookTimer.transform.localScale.

            cookTimer.transform.localScale = new Vector3(Mathf.Lerp(0f, 1f, remainingTime/Cooktime ) , cookTimer.transform.localScale.y, cookTimer.transform.localScale.z);
            //cooking can continue if only station (parent object) has fuel and running.
            // referencing parent object like this each update might be a bad idea.
            yield return new WaitUntil(() =>  transform.GetComponentInParent<Station>().StationActive == true);
            remainingTime = k;
            //if the parent object is active then cooking can continue.
            yield return new WaitForEndOfFrame();
        }
        CookEnded();
    }

    public void CookEnded()
    {
        // put food in items dictionary at statmanager
        StatManager.Stats.IncreaseItemBy ( transform.GetComponentInParent<Station>().StationType , CookAmount );

        Debug.Log( transform.GetComponentInParent<Station>().StationType + " is Cooked" );
        gameObject.GetComponent<SpriteRenderer>().color = new Color(0.99f, 0.1f, 0.1f, 0.33f);
        isSpotEmpty = true;
        // remove visual
    }
}
