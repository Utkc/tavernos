﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWood : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnMouseDown()
    {
        Debug.Log("Add Wood Clicked");
        gameObject.GetComponentInParent<StationManager>().AddWood();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
