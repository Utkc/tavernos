﻿using System.Collections;
using UnityEngine;

public class Station : MonoBehaviour
{
    public string StationType;
    [HideInInspector]
    public bool StationActive;
    [HideInInspector]
    public StationTimer StationT;

    public Sprite Activated;
    public Sprite Deactivated;
    public Sprite FoodOn;
    public Sprite FoodOff;

    SpriteRenderer sr;

    [Header("--------0--------")]
    public GameObject WoodAdder;
    public GameObject FoodAdder;
    public GameObject CookSprite;

    public void ActivateTheStation()
    {
        JukeBox.Singleton.StartPlayingLoop("FireBurn");
        sr.sprite = Activated;
    }

    public void DeactivateTheStation()
    {
        JukeBox.Singleton.FadePausePlayingLoop("FireBurn");
        sr.sprite = Deactivated;
    }

    private void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {

    }

    public float ControllerFadeOutRemainingTime;
    Coroutine coru;
    public void ReInitControllersFadeOutTimer()
    {
        MouseEntered();
        ControllerFadeOutRemainingTime = 1f;
        if (coru != null)
        {
            StopCoroutine(coru);
            coru = StartCoroutine( FadeOutWaitMore() );
        }
        else
            coru = StartCoroutine( FadeOutWaitMore() );
    }

    IEnumerator FadeOutWaitMore()
    {
        while (ControllerFadeOutRemainingTime > 0)
        {
            ControllerFadeOutRemainingTime -= Time.deltaTime ;
            yield return new WaitForEndOfFrame();
        }
        MouseLeft();
    }

    public void MouseLeft()
    {
        // FadeOut Timer
        // gameObject.GetComponentInChildren<StationTimer>().gameObject.GetComponent<Animator>().Play("StationTimerFadeOut");

        FoodAdder.GetComponent<Animator>().Play("StationControlsFadeOut");
        WoodAdder.GetComponent<Animator>().Play("StationControlsFadeOut");
    }

    public void MouseEntered()
    {        
        FoodAdder.GetComponent<Animator>().Play("StationControlsFadeIn");
        WoodAdder.GetComponent<Animator>().Play("StationControlsFadeIn");
    }


    public void StationAddWood()
    {
        OnRightClick();
    }

    public void StationAddFood()
    {
        OnLeftClick();

    }

    void OnRightClick()
    {
        Debug.Log("Right station click");
        if (StatManager.Stats.ConsumeItem("Wood"))
        {
            transform.GetComponentInChildren<StationTimer>().ReActivateTheStation();
            ActivateTheStation();
            JukeBox.Singleton.playFromListOS("AddWood");
        }
        else
        {
            Debug.Log("No Wood");
        }
    }

    void OnLeftClick()
    {
        // adding food
        Debug.Log("Flat Station Click Adding Food");
        if ( transform.GetComponentInChildren<Cooking>().isSpotEmpty && StatManager.Stats.ConsumeItem("Gold") )
        {
            Debug.Log("Spot Empty and we have money start cooking");
            transform.GetComponentInChildren<Cooking>().StartCooking();
            JukeBox.Singleton.playFromListOS(StationType);
        }
        else if (!transform.GetComponentInChildren<Cooking>().isSpotEmpty && StatManager.Stats.items["Gold"] > 0 )
        {
            Debug.Log("We have money , Something is already getting cooked");
        }
        else if (!transform.GetComponentInChildren<Cooking>().isSpotEmpty && !(StatManager.Stats.items["Gold"] > 0) )
        {
            Debug.Log("No Money and Something is already getting cooked");
        }
        else
        {
            Debug.Log("What's going on?");
            Debug.Log("GetComponentInChildren<Cooking>().isSpotEmpty = " + transform.GetComponentInChildren<Cooking>().isSpotEmpty);
            Debug.Log("StatManager.Stats.ConsumeItem(Gold)"+ StatManager.Stats.ConsumeItem("Gold") );
        }
    }
}
