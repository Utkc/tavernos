﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddFood : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnMouseDown()
    {
        gameObject.GetComponentInParent<StationManager>().AddFood();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
