﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour
{

    public Sprite FishPlateSprite;
    public Sprite BeerPlateSprite;
    public Sprite BreadPlateSprite;


    SpriteRenderer srC;
    bool PlateVisible;

    // platevisible & src.enabled SHITSHOW

    public void SpawnPlate (Customers.OrderType ord)
    {
        gameObject.GetComponent<Animator>().Play("plate");
        if (PlateVisible)
        {
            srC.enabled = true;
        }
        else
        {
            PlateVisible = true;
            srC.enabled = true;
        }
        //optimise !!!
        if (ord == Customers.OrderType.Bread)
        {
            Debug.Log("Plate Spawned " + ord.ToString() );
            srC.sprite = BreadPlateSprite;
        }
        if (ord == Customers.OrderType.Beer)
        {
            Debug.Log("Plate Spawned " + ord.ToString() );
            srC.sprite = BeerPlateSprite;
        }
        if (ord == Customers.OrderType.Fish)
        {
            Debug.Log("Plate Spawned " + ord.ToString() );
            srC.sprite = FishPlateSprite;
        }

    }

    public void DisappearPlate()
    {
        PlateVisible = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = PlateVisible;
    }

    // Start is called before the first frame update
    void Start()
    {
        PlateVisible = false;
        srC = transform.GetComponent<SpriteRenderer>();
        srC.enabled = PlateVisible;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
