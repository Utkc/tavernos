﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seat : MonoBehaviour
{
    public bool isEmpty;
    public bool northFacing;
    public bool westFacing;

    private void Start()
    {
        setStatus(true);
    }

    public bool isSeatEmpty()
    {
        return isEmpty;
    }

    public void setStatus (bool status)
    {
        isEmpty = status;
    }

    public void Update()
    {
        if ( isSeatEmpty() )
        {
            //gameObject.GetComponent<SpriteRenderer>().material.color = new Color(0f, 255f, 0f,0.05f);
        }
        else
        {
            //gameObject.GetComponent<SpriteRenderer>().material.color = new Color(255f, 0f, 0f, 0.05f);
        }
    }

}
